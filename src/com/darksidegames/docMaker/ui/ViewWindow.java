/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.darksidegames.docMaker.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.tree.DefaultMutableTreeNode;

import com.darksidegames.docMaker.DebugManager;
import com.darksidegames.docMaker.json.JSONClassModel;
import com.darksidegames.docMaker.json.JSONFunctionModel;
import com.darksidegames.docMaker.json.JSONModel;
import com.darksidegames.docMaker.json.JSONPackageModel;


/**
 * La fenêtre qui permet de modifier les éléments de la documentation.
 * 
 * @author winterskill
 */
public class ViewWindow extends JFrame
{
	protected boolean isComponent;
	
	protected JPanel panel = new JPanel();
	protected BorderLayout layout = new BorderLayout();
	
	protected JPanel content_panel = new JPanel();
	protected GridLayout content_layout = new GridLayout();
	
	protected JPanel     ct_name_panel =             new JPanel();
	protected JLabel     ct_name_label =             new JLabel("Nom");
	protected JTextField ct_name_input =             new JTextField();
	protected JPanel     ct_description_panel =      new JPanel();
	protected JLabel     ct_description_label =      new JLabel("Description");
	protected JTextArea  ct_description_input =      new JTextArea();
	protected JPanel     ct_since_panel =            new JPanel();
	protected JLabel     ct_since_label =            new JLabel("Since");
	protected JTextField ct_since_input =            new JTextField();
	protected JPanel     ct_returnValue_panel =      new JPanel();
	protected JLabel     ct_returnValue_label =      new JLabel("Valeur de retour");
	protected JTextField ct_returnValue_input =      new JTextField();
	protected JPanel     ct_isref_panel =            new JPanel();
	protected JLabel     ct_isref_label =            new JLabel("Référence ?");
	protected JCheckBox  ct_isref_input =            new JCheckBox();
	protected JPanel     ct_pointer_panel =          new JPanel();
	protected JLabel     ct_pointer_label =          new JLabel("Pointeur ?");
	protected JCheckBox  ct_pointer_input =          new JCheckBox();
	protected JPanel     ct_shortDescription_panel = new JPanel();
	protected JLabel     ct_shortDescription_label = new JLabel("Description courte");
	protected JTextArea  ct_shortDescription_input = new JTextArea();
	protected JPanel     ct_deprecated_panel =       new JPanel();
	protected JLabel     ct_deprecated_label =       new JLabel("Obsolète ?");
	protected JCheckBox  ct_deprecated_input =       new JCheckBox();
	protected JPanel     ct_author_panel =           new JPanel();
	protected JLabel     ct_author_label =           new JLabel("Auteur");
	protected JTextField ct_author_input =           new JTextField();
	protected JPanel     ct_istemplate_panel =       new JPanel();
	protected JLabel     ct_istemplate_label =       new JLabel("Est un template ?");
	protected JCheckBox  ct_istemplate_input =       new JCheckBox();
	
	protected JPanel buttons_panel = new JPanel();
	protected GridLayout buttons_layout = new GridLayout(1, 3);
	protected JButton button_apply = new JButton("Appliquer");
	protected JButton button_cancel = new JButton("Annuler");
	protected JButton button_apply_close = new JButton("Appliquer & Fermer");
	
	protected int components_count = 0;
	protected int textsCols = 50;
	
	protected DefaultMutableTreeNode element;
	
	/**
	 * 
	 * @param element L'element cliqué de l'arbre.
	 * @param component Si TRUE, alors on a donné un composant de la
	 *                  documentation (ex : un {@code JSONTypedefModel}). Sinon
	 *                  c'est qu'on a donné une étiquette (ex : "Fonctions").
	 */
	public ViewWindow(DefaultMutableTreeNode element, boolean component)
	{
		this.element = element;
		this.isComponent = component;
		setTitle("Propriétés de " + element.toString());
		setSize(500, 500);
		setLocationRelativeTo(null);
		
		content_panel.setLayout(content_layout);
		buttons_panel.setLayout(buttons_layout);
		panel.setLayout(layout);
		panel.add(new JScrollPane(content_panel), BorderLayout.CENTER);
		panel.add(buttons_panel, BorderLayout.SOUTH);
		getContentPane().add(panel);
		
		repaintPanel();
		
		this.setVisible(true);
	}
	
	/**
	 * Met à jour les composants de la fenêtre.
	 */
	protected void repaintPanel()
	{
		DebugManager.print("com.darksidegames.docMaker.ui:ViewWindow@repaintPanel", "Repaint panel");
		panel.removeAll();
		content_panel.removeAll();
		buttons_panel.removeAll();
		
		if (isComponent) {
			if (((JSONModel)element.getUserObject()).__type == JSONPackageModel.class) {
				JSONPackageModel json = (JSONPackageModel) element.getUserObject();
				ct_name_input.setText(json.name);
				ct_name_input.setColumns(textsCols);
				ct_name_panel.add(ct_name_label);
				ct_name_panel.add(ct_name_input);
				content_panel.add(ct_name_panel);
				components_count++;
				ct_description_input.setText(json.description);
				ct_description_input.setColumns(textsCols);
				ct_description_panel.add(ct_description_label);
				ct_description_panel.add(ct_description_input);
				content_panel.add(ct_description_panel);
				components_count++;
				ct_since_input.setText(json.since);
				ct_since_input.setColumns(textsCols);
				ct_since_panel.add(ct_since_label);
				ct_since_panel.add(ct_since_input);
				content_panel.add(ct_since_panel);
				components_count++;
			} else if (((JSONModel)element.getUserObject()).__type == JSONFunctionModel.class) {
				JSONFunctionModel json = (JSONFunctionModel) element.getUserObject();
				ct_name_input.setText(json.name);
				ct_name_input.setColumns(textsCols);
				ct_name_panel.add(ct_name_label);
				ct_name_panel.add(ct_name_input);
				content_panel.add(ct_name_panel);
				components_count++;
				ct_returnValue_input.setText(json.returnValue);
				ct_returnValue_input.setColumns(textsCols);
				ct_returnValue_panel.add(ct_returnValue_label);
				ct_returnValue_panel.add(ct_returnValue_input);
				content_panel.add(ct_returnValue_panel);
				components_count++;
				ct_isref_input.setSelected(json.ref);
				ct_isref_panel.add(ct_isref_label);
				ct_isref_panel.add(ct_isref_input);
				content_panel.add(ct_isref_panel);
				components_count++;
				ct_pointer_input.setSelected(json.pointer);
				ct_pointer_panel.add(ct_pointer_label);
				ct_pointer_panel.add(ct_pointer_input);
				content_panel.add(ct_pointer_panel);
				components_count++;
				ct_shortDescription_input.setText(json.shortDescription);
				ct_shortDescription_input.setColumns(textsCols);
				ct_shortDescription_panel.add(ct_shortDescription_label);
				ct_shortDescription_panel.add(ct_shortDescription_input);
				content_panel.add(ct_shortDescription_panel);
				components_count++;
				ct_description_input.setText(json.description);
				ct_description_input.setColumns(textsCols);
				ct_description_panel.add(ct_description_label);
				ct_description_panel.add(ct_description_input);
				content_panel.add(ct_description_panel);
				components_count++;
				ct_since_input.setText(json.since);
				ct_since_input.setColumns(textsCols);
				ct_since_panel.add(ct_since_label);
				ct_since_panel.add(ct_since_input);
				content_panel.add(ct_since_panel);
				components_count++;
				ct_deprecated_input.setSelected(json.deprecated);
				ct_deprecated_panel.add(ct_deprecated_label);
				ct_deprecated_panel.add(ct_deprecated_input);
				content_panel.add(ct_deprecated_panel);
				components_count++;
				ct_author_input.setText(json.author);
				ct_author_input.setColumns(textsCols);
				ct_author_panel.add(ct_author_label);
				ct_author_panel.add(ct_author_input);
				content_panel.add(ct_author_panel);
				components_count++;
				ct_istemplate_input.setSelected(json.template);
				ct_istemplate_panel.add(ct_istemplate_label);
				ct_istemplate_panel.add(ct_istemplate_input);
				content_panel.add(ct_istemplate_panel);
				components_count++;
			}
		}
		
		content_layout.setHgap(10);
		content_layout.setVgap(10);
		content_layout.setColumns(1);
		content_layout.setRows(components_count);
		
		buttons_panel.add(button_apply_close);
		buttons_panel.add(button_apply);
		buttons_panel.add(button_cancel);
		
		panel.add(new JScrollPane(content_panel), BorderLayout.CENTER);
		panel.add(buttons_panel, BorderLayout.SOUTH);
		
		content_panel.validate();
		content_panel.repaint();
		buttons_panel.validate();
		buttons_panel.repaint();
		panel.validate();
		panel.repaint();
	}
}
