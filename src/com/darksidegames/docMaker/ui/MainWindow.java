/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.darksidegames.docMaker.ui;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.darksidegames.docMaker.DebugManager;
import com.darksidegames.docMaker.Documentation;
import com.darksidegames.docMaker.config.ExitCodes;
import com.darksidegames.docMaker.config.Languages;
import com.darksidegames.docMaker.entryPoint.EntryPoint;
import com.darksidegames.docMaker.json.JSONAnnotationElementModel;
import com.darksidegames.docMaker.json.JSONClassMemberModel;
import com.darksidegames.docMaker.json.JSONClassMethodModel;
import com.darksidegames.docMaker.json.JSONClassModel;
import com.darksidegames.docMaker.json.JSONFunctionModel;
import com.darksidegames.docMaker.json.JSONInterfaceModel;
import com.darksidegames.docMaker.json.JSONPackageModel;
import com.darksidegames.docMaker.json.JSONPreprocessorConstantModel;
import com.darksidegames.docMaker.json.JSONTypedefModel;
import com.darksidegames.docMaker.json.JSONVariableModel;
import com.darksidegames.docMaker.json.calypso.JSONCalypsoSuitcaseModel;

import static com.darksidegames.docMaker.util.IOUtils.fread;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.awt.BorderLayout;


/**
 * La fenêtre principale de l'interface.
 * 
 * @author winterskill
 * @since 1.1.0pre1
 */
public class MainWindow extends JFrame
{
	protected JMenuBar menubar = new JMenuBar();
	protected JMenu menu_file = new JMenu("Fichier");
	protected JMenuItem menu_file_newDoc = new JMenuItem("Nouvelle documentation");
	protected JMenuItem menu_file_openDoc = new JMenuItem("Ouvrir une documentation");
	protected JMenuItem menu_file_quit = new JMenuItem("Quitter");
	protected JMenu menu_help = new JMenu("?");
	protected JMenuItem menu_help_about = new JMenuItem("A propos");
	protected JMenuItem menu_help_licence = new JMenuItem("Licence");
	
	protected JTree doctree;
	
	protected JPanel panel = new JPanel();
	protected BorderLayout layout = new BorderLayout();
	protected GridLayout centerLayout = new GridLayout(1, 2);
	protected JPanel centerPanel = new JPanel();
	protected GridLayout choicesLayout = new GridLayout(3, 1);
	protected JPanel choicesPanel = new JPanel();
	
	protected JButton choice_delete = new JButton("Supprimer");
	protected JButton choice_view = new JButton("Propriétés");
	protected JButton choice_new = new JButton("Nouvel élément");
	
	public MainWindow()
	{
		DebugManager.print("com.darksidegames.docMaker.ui:MainWindow@MainWindow", "Creating new main window");
		setTitle("Fenetre principale");
		setSize(500, 500);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		menu_file_newDoc.addActionListener(new menu_file_newDocListener());
		menu_file_openDoc.addActionListener(new menu_file_openDocListener());
		menu_file_quit.addActionListener(new menu_file_quitListener());
		menu_help_about.addActionListener(new menu_help_aboutListener());
		menu_help_licence.addActionListener(new menu_help_licenceListener());
		menu_file.add(menu_file_newDoc);
		menu_file.add(menu_file_openDoc);
		menu_file.add(new JSeparator());
		menu_file.add(menu_file_quit);
		menu_help.add(menu_help_about);
		menu_help.add(menu_help_licence);
		menubar.add(menu_file);
		menubar.add(menu_help);
		setJMenuBar(menubar);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent windowEvent)
			{
				DebugManager.print("com.darksidegames.docMaker.ui:MainWindow@MainWindow:WindowAdapter@windowClosing", "CLOSE");
				dispose();
				EntryPoint.quitProgram(ExitCodes.STANDARD.getCode());
			}
		});
		
		choice_view.addActionListener(new choice_viewListener());
		choicesPanel.setLayout(choicesLayout);
		centerPanel.setLayout(centerLayout);
		panel.add(centerPanel, BorderLayout.CENTER);
		panel.setLayout(layout);
		getContentPane().add(panel);
		
		buildTree();
		repaintPanel();
		
		setVisible(true);
	}
	
	/**
	 * Met à jour les composants de la fenêtre.
	 */
	protected void repaintPanel()
	{
		DebugManager.print("com.darksidegames.docMaker.ui:MainWindow@repaintPanel", "Repaint panel");
		centerPanel.removeAll();
		choicesPanel.removeAll();
		panel.removeAll();
		
		choicesLayout.setHgap(5);
		choicesLayout.setVgap(5);
		choicesPanel.add(choice_delete);
		choicesPanel.add(choice_view);
		choicesPanel.add(choice_new);
		
		centerPanel.add(new JScrollPane(doctree));
		centerPanel.add(choicesPanel);
		panel.add(centerPanel, BorderLayout.CENTER);
		
		centerPanel.validate();
		centerPanel.repaint();
		choicesPanel.validate();
		choicesPanel.repaint();
		panel.validate();
		panel.repaint();
	}
	
	/**
	 * Construit l'arbre des éléments de la documentation.
	 */
	public void buildTree()
	{
		DebugManager.print("com.darksidegames.docMaker.ui:MainWindow@buildTree", "Building documentation tree");
		if (EntryPoint.actualDocumentation != null) {
			// pour éviter les NullPointerException
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow@buildTree", "not null");
			DefaultMutableTreeNode root = new DefaultMutableTreeNode(EntryPoint.actualDocumentation.sourceDatas);
			DefaultMutableTreeNode packagesNode = new DefaultMutableTreeNode("Packages");
			List<JSONPackageModel> packages = EntryPoint.actualDocumentation.sourceDatas.packages;
			for (int i = 0; i < packages.size(); i++) {
				DefaultMutableTreeNode nPackage = new DefaultMutableTreeNode(packages.get(i));
				if (packages.get(i).functions != null && packages.get(i).functions.size() != 0) {
					// les fonctions
					List<JSONFunctionModel> functions = packages.get(i).functions;
					DefaultMutableTreeNode nPackageFunctions = new DefaultMutableTreeNode("Fonctions");
					for (int j = 0; j < functions.size(); j++) {
						DefaultMutableTreeNode nFunction = new DefaultMutableTreeNode(functions.get(j));
						nPackageFunctions.add(nFunction);
					}
					nPackage.add(nPackageFunctions);
				}
				if (packages.get(i).classes != null && packages.get(i).classes.size() != 0) {
					// les classes
					List<JSONClassModel> classes = packages.get(i).classes;
					DefaultMutableTreeNode nPackageClasses = new DefaultMutableTreeNode("Classes");
					for (int j = 0; j < classes.size(); j++) {
						DefaultMutableTreeNode nClass = new DefaultMutableTreeNode(classes.get(j));
						
						if (classes.get(j).members != null && classes.get(j).members.size() != 0) {
							// les membres
							List<JSONClassMemberModel> members = classes.get(j).members;
							DefaultMutableTreeNode nClassMembers = new DefaultMutableTreeNode("Membres");
							for (int k = 0; k < members.size(); k++) {
								DefaultMutableTreeNode nMember = new DefaultMutableTreeNode(members.get(k));
								nClassMembers.add(nMember);
							}
							nClass.add(nClassMembers);
						}
						
						if (classes.get(j).methods != null && classes.get(j).methods.size() != 0) {
							// les méthodes
							List<JSONClassMethodModel> methods = classes.get(j).methods;
							DefaultMutableTreeNode nClassMethods = new DefaultMutableTreeNode("Méthodes");
							for (int k = 0; k < methods.size(); k++) {
								DefaultMutableTreeNode nMethod = new DefaultMutableTreeNode(methods.get(k));
								nClassMethods.add(nMethod);
							}
							nClass.add(nClassMethods);
						}
						nPackageClasses.add(nClass);
					}
					nPackage.add(nPackageClasses);
				}
				
				if (packages.get(i).interfaces != null && packages.get(i).interfaces.size() != 0) {
					// les interfaces
					List<JSONInterfaceModel> interfaces = packages.get(i).interfaces;
					DefaultMutableTreeNode nInterfaces = new DefaultMutableTreeNode("Interfaces");
					
					for (int j = 0; j < interfaces.size(); j++) {
						DefaultMutableTreeNode nInterface = new DefaultMutableTreeNode(interfaces.get(j));
						
						if (interfaces.get(j).members != null && interfaces.get(j).members.size() != 0) {
							// les membres
							List<JSONClassMemberModel> members = interfaces.get(j).members;
							DefaultMutableTreeNode nClassMembers = new DefaultMutableTreeNode("Membres");
							for (int k = 0; k < members.size(); k++) {
								DefaultMutableTreeNode nMember = new DefaultMutableTreeNode(members.get(k));
								nClassMembers.add(nMember);
							}
							nInterface.add(nClassMembers);
						}
						
						if (interfaces.get(j).methods != null && interfaces.get(j).methods.size() != 0) {
							// les méthodes
							List<JSONClassMethodModel> methods = interfaces.get(j).methods;
							DefaultMutableTreeNode nMethods = new DefaultMutableTreeNode("Méthodes");
							for (int k = 0; k < methods.size(); k++) {
								DefaultMutableTreeNode nMethod = new DefaultMutableTreeNode(methods.get(k));
								nMethods.add(nMethod);
							}
							nInterface.add(nMethods);
						}
						nInterfaces.add(nInterface);
					}
					
					nPackage.add(nInterfaces);
				}
				
				if (packages.get(i).variables != null && packages.get(i).variables.size() != 0) {
					// les variables
					List<JSONVariableModel> variables = packages.get(i).variables;
					DefaultMutableTreeNode nVariables = new DefaultMutableTreeNode("Variables");
					
					for (int j = 0; j < variables.size(); j++) {
						DefaultMutableTreeNode nVariable = new DefaultMutableTreeNode(variables.get(j));
						nVariables.add(nVariable);
					}
					
					nPackage.add(nVariables);
				}
				
				if (packages.get(i).annotations != null && packages.get(i).annotations.size() != 0) {
					// les annotations
					List<JSONAnnotationElementModel> annotations = packages.get(i).annotations;
					DefaultMutableTreeNode nAnnotations = new DefaultMutableTreeNode("Annotations");
					
					for (int j = 0; j < annotations.size(); j++) {
						DefaultMutableTreeNode nAnnotation = new DefaultMutableTreeNode(annotations.get(j));
						nAnnotations.add(nAnnotation);
					}
					
					nPackage.add(nAnnotations);
				}
				
				if (packages.get(i).preprocessorConstants != null && packages.get(i).preprocessorConstants.size() != 0) {
					// les constantes de préprocesseur
					List<JSONPreprocessorConstantModel> preprocessorConstants = packages.get(i).preprocessorConstants;
					DefaultMutableTreeNode nPreprocessorConstants = new DefaultMutableTreeNode("Constantes de préprocesseur");
					
					for (int j = 0; j < preprocessorConstants.size(); j++) {
						DefaultMutableTreeNode nPreprocessorConstant = new DefaultMutableTreeNode(preprocessorConstants.get(j));
						nPreprocessorConstants.add(nPreprocessorConstant);
					}
					
					nPackage.add(nPreprocessorConstants);
				}
				
				if (packages.get(i).typedefs != null && packages.get(i).typedefs.size() != 0) {
					// les typedefs
					List<JSONTypedefModel> typedefs = packages.get(i).typedefs;
					DefaultMutableTreeNode nTypedefs = new DefaultMutableTreeNode("Typedefs");
					
					for (int j = 0; j < typedefs.size(); j++) {
						DefaultMutableTreeNode nTypedef = new DefaultMutableTreeNode(typedefs.get(j));
						nTypedefs.add(nTypedef);
					}
					
					nPackage.add(nTypedefs);
				}
				
				// ===== LANGAGE CALYPSO =====
				if (EntryPoint.actualDocumentation.getLanguage() == Languages.CALYPSO) {
					if (packages.get(i).suitcases != null && packages.get(i).suitcases.size() != 0) {
						// les suitcases
						List<JSONCalypsoSuitcaseModel> suitcases = packages.get(i).suitcases;
						DefaultMutableTreeNode nSuitcases = new DefaultMutableTreeNode("Suitcases");
						
						for (int j = 0; j < suitcases.size(); j++) {
							DefaultMutableTreeNode nSuitcase = new DefaultMutableTreeNode(suitcases.get(j));
							nSuitcases.add(nSuitcase);
						}
						
						nPackage.add(nSuitcases);
					}
				}
				packagesNode.add(nPackage);
			}
			root.add(packagesNode);
			
			doctree = new JTree(root);
		} else {
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow@buildTree", "null");
			DefaultMutableTreeNode root = new DefaultMutableTreeNode("<NONE>");
			doctree = new JTree(root);
		}
		repaintPanel();
	}
	
	public class menu_file_quitListener implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:menu_file_quitListener@actionPerformed", "Quit");
			EntryPoint.quitProgram(ExitCodes.STANDARD.getCode());
		}
	}
	
	public class menu_file_newDocListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:menu_file_newDocListener@actionPerformed", "New documentation");
			EntryPoint.actualDocumentation = new Documentation(null, Languages.CALYPSO);
			// TODO faire une popup pour sélectionner les infos basiques de la nouvelle documentation à créer (nom, langage...)
		}
	}
	
	public class menu_file_openDocListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:menu_file_openDocListener@actionPerformed", "Openning documentation");
			JFileChooser jfc = new JFileChooser();
			jfc.setCurrentDirectory(new File("."));
			int jfc_retval = jfc.showOpenDialog(null);
			
			File file = null;
			if (jfc_retval == JFileChooser.APPROVE_OPTION) {
				DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:menu_file_openDocListener@actionPerformed", "Approve");
				file = jfc.getSelectedFile();
			} else if (jfc_retval == JFileChooser.CANCEL_OPTION) {
				DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:menu_file_openDocListener@actionPerformed", "Cancel");
				return;
			} else {
				DebugManager.sendErrorMessage("ERROR", "Invalid file", true);
				return;
			}
			
			try {
				EntryPoint.actualDocumentation = new Documentation(fread(file.getPath()));
				buildTree();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				EntryPoint.quitProgram(ExitCodes.DOCUMENTATION_DATAS_FILE_DOES_NOT_EXISTS.getCode());
			}
		}
	}
	
	public class menu_help_aboutListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:menu_help_aboutListener@actionPerformed", "Clicked");
			DebugManager.sendErrorMessage("", "LOL rien à afficher pour le moment", false);
		}
	}
	
	public class menu_help_licenceListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:menu_help_licenceListener@actionPerformed", "Clicked");
			LicenceWindow lw = new LicenceWindow();
		}
	}
	
	public class choice_viewListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			DebugManager.print("com.darksidegames.docMaker.ui:MainWindow:choice_viewListener@actionPerformed", "Clicked");
			boolean isString = true;
			try {
				DefaultMutableTreeNode dmte = (DefaultMutableTreeNode) doctree.getLastSelectedPathComponent();
				String s = (String) dmte.getUserObject();
			} catch (ClassCastException cce) {
				isString = false;
			}
			if (!isString) {
				ViewWindow vw = new ViewWindow((DefaultMutableTreeNode) doctree.getLastSelectedPathComponent(), true);
			} else {
				DebugManager.sendErrorMessage("Error", "Impossible de voir les propriétés d'une étiquette", true);
			}
		}
	}
}
