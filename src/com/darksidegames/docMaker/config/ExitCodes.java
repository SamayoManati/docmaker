/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.config;

/**
 * Contient les codes de fermeture de l'application.
 * 
 * @author winterskill
 */
public enum ExitCodes
{
	DOCUMENTATION_GIVEN_LANGUAGE_DOES_NOT_EXISTS(-4),
	DOCUMENTATION_DATAS_FILE_DOES_NOT_EXISTS(-3),
	NO_OUTPUT_FOLDER_GIVEN(-2),
	NO_DOCUMENTATION_DATAS_GIVEN(-1),
	STANDARD(0),
	PARAM_VERSION_GIVEN(1),
	PARAM_HELP_GIVEN(2),
	OMAE_WA_MO_SHINDEIRU(3);
	
	private int code;
	
	private ExitCodes(int code)
	{
		this.code = code;
	}
	
	public int getCode()
	{
		return this.code;
	}
}