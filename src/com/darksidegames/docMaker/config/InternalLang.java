/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.config;

import com.darksidegames.docMaker.exceptions.TermNotFoundException;
import com.darksidegames.docMaker.DebugManager;

import java.util.HashMap;


/**
 * La langue interne est une langue de lexique codée en dur dans l'application,
 * afin de posséder tout le temps une langue pour afficher les interfaces
 * graphiques et à mettre dans la documentation, même si il n'y a aucune langue
 * de disponible dans le dossier locales.
 * 
 * @author winterskill
 */
public abstract class InternalLang
{
	protected static HashMap<String, String> secLang;
	protected static HashMap<String, String> secOutput;
	protected static HashMap<String, String> secUI;
	protected static boolean initialized = false;
	
	protected static void staticInit()
	{
		DebugManager.print("com.darksidegames.docMaker.config:InternalLang@staticInit", "Static initialization...");
		
		// initialisation statique
		if (!InternalLang.initialized) {
			DebugManager.print("com.darksidegames.docMaker.config:InternalLang@staticInit", "Class never initialized."
					+ " Initialization.");
			
			initialized = true;
			
			InternalLang.secLang = new HashMap<String, String>();
			InternalLang.secOutput = new HashMap<String, String>();
			InternalLang.secUI = new HashMap<String, String>();

			// remplissage
			InternalLang.secLang.put("lang", "<INTERNAL_LANG>");
			
			InternalLang.secOutput.put("msg.deprecated.title", "<b>obsolète</b>");
			InternalLang.secOutput.put("msg.deprecated.content", "élément obsolète");
			InternalLang.secOutput.put("term.minVersion", "version min");
			InternalLang.secOutput.put("term.version", "version");
			InternalLang.secOutput.put("term.annotations", "annotations");
			InternalLang.secOutput.put("term.returnValue", "valeur de retour");
			InternalLang.secOutput.put("term.reference", "référence");
			InternalLang.secOutput.put("term.yes", "yep");
			InternalLang.secOutput.put("term.no", "nop");
			InternalLang.secOutput.put("term.author", "auteur");
			InternalLang.secOutput.put("term.abstract.m", "abstrait");
			InternalLang.secOutput.put("term.abstract.f", "abstraite");
			InternalLang.secOutput.put("term.final.m", "final");
			InternalLang.secOutput.put("term.final.f", "finale");
			InternalLang.secOutput.put("term.extends", "étends");
			InternalLang.secOutput.put("term.implements", "implémente");
			InternalLang.secOutput.put("term.returnRef", "retourne une référence");
			InternalLang.secOutput.put("term.scope", "portée");
			InternalLang.secOutput.put("term.type", "type");
			InternalLang.secOutput.put("term.constant.m", "constant");
			InternalLang.secOutput.put("term.constant.f", "constante");
			InternalLang.secOutput.put("term.pointer", "pointeur");
			InternalLang.secOutput.put("term.value", "valeur");
			InternalLang.secOutput.put("term.none.f", "aucune");
			InternalLang.secOutput.put("term.none.m", "aucun");
			InternalLang.secOutput.put("term.function", "fonction");
			InternalLang.secOutput.put("term.class", "classe");
			InternalLang.secOutput.put("term.interface", "interface");
			InternalLang.secOutput.put("term.variable", "variable");
			InternalLang.secOutput.put("term.annotation", "annotation");
			InternalLang.secOutput.put("term.preprocessorConstant", "constante de préprocesseur");
			InternalLang.secOutput.put("term.package", "package");
			InternalLang.secOutput.put("term.method", "méthode");
			InternalLang.secOutput.put("term.constructor", "constructeur");
			InternalLang.secOutput.put("term.destructor", "destructeur");
			InternalLang.secOutput.put("term.exceptions", "exceptions");
			InternalLang.secOutput.put("term.static", "statique");
			InternalLang.secOutput.put("term.returnPointer", "retourne un pointeur");
			InternalLang.secOutput.put("term.typedef", "typedef");
			InternalLang.secOutput.put("term.template", "template");
			InternalLang.secOutput.put("term.templateArguments", "arguments de template");
			InternalLang.secOutput.put("title.informations", "informations");
			InternalLang.secOutput.put("title.functions", "fonctions");
			InternalLang.secOutput.put("title.classes", "classes");
			InternalLang.secOutput.put("title.interfaces", "interfaces");
			InternalLang.secOutput.put("title.variables", "variables");
			InternalLang.secOutput.put("title.annotations", "annotations");
			InternalLang.secOutput.put("title.preprocessorConstants", "constantes de préprocesseur");
			InternalLang.secOutput.put("title.arguments", "arguments");
			InternalLang.secOutput.put("title.members", "membres");
			InternalLang.secOutput.put("title.methods", "méthodes");
			InternalLang.secOutput.put("title.constructors", "constructeurs");
			InternalLang.secOutput.put("title.destructors", "destructeurs");
			InternalLang.secOutput.put("title.keys", "clefs");
			InternalLang.secOutput.put("title.typedefs", "typedefs");
			InternalLang.secOutput.put("title.templateArguments", "arguments de template");
			InternalLang.secOutput.put("label.new", "new");
			
			InternalLang.secOutput.put("calypso.term.suitcase", "suitcase");
			InternalLang.secOutput.put("calypso.title.suitcases", "suitcases");

			InternalLang.secUI.put("button.ok", "OK");
			InternalLang.secUI.put("button.saveLicenceIntoFile", "Save into file");
		} else {
			DebugManager.print("com.darksidegames.docMaker.config:InternalLang@staticInit", "Class already initialized."
					+ " Canceling initialization.", DebugManager.MESSAGE_ERROR);
		}
	}
	
	/**
	 * Retourne un terme dans la langue interne.
	 * 
	 * @param termname
	 * Le nom du terme.
	 * @param section
	 * La section du terme.
	 * @return
	 * Le terme.
	 * 
	 * @throws TermNotFoundException
	 * Quand termname ou section n'existe pas.
	 */
	public static String get(String termname, String section) throws TermNotFoundException
	{
		DebugManager.print("com.darksidegames.docMaker.config:InternalLang@get", "Method call");
		InternalLang.staticInit();
		
		switch (section)
		{
		case "lang":
			if (InternalLang.secLang.containsKey(termname))
				return InternalLang.secLang.get(termname);
			else
				throw new TermNotFoundException("term " + termname + " not found");
		case "output":
			if (InternalLang.secOutput.containsKey(termname))
				return InternalLang.secOutput.get(termname);
			else
				throw new TermNotFoundException("term " + termname + " not found");
		case "ui":
			if (InternalLang.secUI.containsKey(termname))
				return InternalLang.secUI.get(termname);
			else
				throw new TermNotFoundException("term " + termname + " not found");
		default:
			throw new TermNotFoundException("section " + section + " not found");
		}
	}
	
	/**
	 * Retourne si un terme existe dans la langue interne.
	 * 
	 * @param termname
	 * Le nom du terme.
	 * @param section
	 * La section du terme.
	 * 
	 * @throws TermNotFoundException
	 * Quand termname ou section n'existe pas
	 */
	public static boolean exists(String termname, String section) throws TermNotFoundException
	{
		DebugManager.print("com.darksidegames.docMaker.config:InternalLang@exists", "Method call");
		InternalLang.staticInit();
		
		switch (section)
		{
		case "lang":
			return InternalLang.secLang.containsKey(termname);
		case "output":
			return InternalLang.secOutput.containsKey(termname);
		case "ui":
			return InternalLang.secUI.containsKey(termname);
		default:
			throw new TermNotFoundException("section " + section + " not found");
		}
	}
}
