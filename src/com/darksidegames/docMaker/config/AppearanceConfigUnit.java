/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.config;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.darksidegames.docMaker.DebugManager;
import com.darksidegames.docMaker.LexiqueManager;
import com.darksidegames.docMaker.exceptions.TermNotFoundException;
import com.darksidegames.docMaker.json.JSONScope;


/**
 * Cette classe de configuration gère l'apparence de la documentation générée.
 * 
 * @author winterskill
 */
public abstract class AppearanceConfigUnit
{
	/**
	 * La longueur maximale des descriptions.
	 */
	public static int descriptionMaxLength = 100;
	/**
	 * La portée par défaut des éléments dans la documentation.
	 */
	public static JSONScope defaultScope = JSONScope.PUBLIC;
	
	/**
	 * L'objet pour manipuler le fichier de configuration {@code appearance.cfg}
	 */
	protected static Properties configFile;
	
	public static void init() throws IOException
	{
		// TODO ce n'est qu'une méthode temporaire, en attendant mieux
		DebugManager.print("com.darksidegames.docMaker.config:AppearenceConfigUnit@init",
				"AppearenceConfigUnit initialization.");
		
		configFile = ConfigUnitUtils.initFile("config/appearance.cfg");
		
		if (configFile.containsKey("general.description.maxLength"))
			AppearanceConfigUnit.descriptionMaxLength = Integer.parseInt(
					configFile.getProperty("general.description.maxLength"));
	}
	
	/**
	 * Retourne le code HTML correspondant au menu d'en-tête de la documentation
	 * générée.
	 * 
	 * @param isElement
	 * La page concernée est-t-elle un élément (donc, dans un sous-dossier), ou
	 * bien n'en n'est-t-elle pas un (donc, à la racine)? Cela permet d'adapter
	 * les chemins d'accès.
	 * @return Le code HTML
	 */
	public static String getHeaderMenu(boolean isElement)
	{
		String elementPrefix = (isElement) ? "../" : "";
		return "<nav class=\"navbar navbar-default\"><div class=\"container-flu"
				+ "id\"><div class=\"navbar-header\"><a class=\"navbar-brand\" "
				+ "href=\"" + elementPrefix + "index.html\">Documentation</a></"
				+ "div><div class=\"collapse navbar-collapse\"><ul class=\"nav n"
				+ "avbar-nav\"><li><a href=\"" + elementPrefix + "packages.html"
				+ "\">Packages</a></li><li><a href=\"" + elementPrefix + "depre"
				+ "cated.html\">Deprecated</a></li></ul></div></div></nav><br/>";
	}
	
	/**
	 * Retourne le code HTML pour une table.
	 * 
	 * @param entries
	 * Une HashMap contenant les entrées de la table à générer.
	 * @param title
	 * Le titre de la table. Mettez <code>""</code> pour signifier "pas de titre".
	 * @return Le code HTML
	 */
	public static String generateTable(HashMap<String, String> entries, String title)
	{
		String retval = "<table class=\"table table-striped datatable\">";
		
		if (!title.equals(""))
			retval += "<thead><tr><th colspan=\"2\"><div class=\"content\">" + title + "</div></th></tr></thead>";
		
		for (Map.Entry<String, String> i : entries.entrySet())
			retval += "<tr><td>" + i.getKey() + "</td><td>" + i.getValue() + "</td></tr>";
		
		return retval;
	}
	
	/**
	 * Retourne le code HTML du label "Nouveau".
	 * 
	 * @return Le code HTML.
	 */
	public static String getLabelNew()
	{
		try {
			return "<span class=\"label label-success\">" + LexiqueManager.getTerm("label.new", "output") + "</span>";
		} catch (TermNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Génère un code HTML pour un label d'informations, utilisé par exemple
	 * pour marquer un constructeur.
	 * 
	 * @param content
	 * L'intitulé du label.
	 * 
	 * @return Le code HTML généré.
	 */
	public static String generateLabel(String content)
	{
		return "<span class=\"label label-info\">" + content + "</span>";
	}
	
	/**
	 * Génère un code HTML pour un breadcrumb contenant les valeurs fournies.
	 * 
	 * @param entries
	 * Les entrées à mettre dans le breadcrumb, sous la forme
	 * <pre>
	 * {
	 *     { nom_affiché, url },
	 *     { nom_affiché, url },
	 *     ...
	 * }
	 * </pre>
	 * @param lastEntryIsActiveEntry
	 * Si <code>true</code>, la dernière entrée du breadcrumb ne sera pas mise
	 * sous forme de lien, pour signifier par exemple qu'il s'agit de la page
	 * active.
	 * 
	 * @return Le code HTML.
	 */
	public static String generateBreadcrumb(String[][] entries, boolean lastEntryIsActiveEntry)
	{
		String retval = "<ol class=\"breadcrumb\">";
		
		for (int i = 0; i < entries.length; i++) {
			if ((i == entries.length - 1) && lastEntryIsActiveEntry) {
				// si on arrive au dernier élément
				retval += "<li class=\"active\">" + entries[i][0] + "</li>";
			} else {
				retval += "<li><a href=\"" + entries[i][1] + "\">" + entries[i][0] + "</a></li>";
			}
		}
		retval += "</ol>";
		
		return retval;
	}
	
	/**
	 * Retourne le code HTML qui affiche un message pour les éléments obsolètes.
	 * 
	 * @return Le code HTML.
	 */
	public static String getDeprecatedWarning()
	{
		try {
			return "<div class=\"alert alert-danger\">"
					+ LexiqueManager.getTerm("msg.deprecated.title", "output")
					+ " : "
					+ LexiqueManager.getTerm("msg.deprecated.content", "output")
					+ "</div>";
		} catch (TermNotFoundException e) {
			e.printStackTrace();
		}
		
		return "";
	}
}
