/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Représente une classe présente dans la documentation.
 * 
 * @author winterskill
 */
public class JSONClassModel extends JSONModel
{
	/**
	 * Le nom de la classe.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La classe est-t-elle abstraite ?
	 */
	@SerializedName("isAbstract")
	public boolean isAbstract = false;
	/**
	 * La classe est-t-elle finale ?
	 */
	@SerializedName("isFinal")
	public boolean isFinal = false;
	/**
	 * La liste des extends de la classe.
	 */
	@SerializedName("extendsList")
	public List<String> extendsList;
	/**
	 * La liste des implémentations de la classe.
	 */
	@SerializedName("implementsList")
	public List<String> implementsList;
	/**
	 * La liste des membres de la classe.
	 */
	@SerializedName("members")
	public List<JSONClassMemberModel> members;
	/**
	 * La liste des méthodes de la classe.
	 */
	@SerializedName("methods")
	public List<JSONClassMethodModel> methods;
	/**
	 * La description courte.
	 */
	@SerializedName("shortDescription")
	public String shortDescription = "";
	/**
	 * La description de la classe.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La version minimale du code où on peut utiliser cette classe.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * La classe est-t-elle obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * L'auteur de la classe.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La liste des annotations présentes sur la classe.
	 */
	@SerializedName("annotations")
	public List<JSONAnnotationModel> annotations;
	/**
	 * La fonction est-t-elle un template ?
	 */
	@SerializedName("template")
	public boolean template = false;
	/**
	 * La liste des arguments du template.
	 * Pris en compte uniquement si template=true.
	 */
	@SerializedName("template_args")
	public List<JSONTemplateArgumentModel> template_args;
	
	public String toString()
	{
		return name;
	}
}
