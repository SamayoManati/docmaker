/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import com.google.gson.annotations.SerializedName;

/**
 * Représente un throws d'une fonction présente dans la documentation.
 * 
 * @author winterskill
 */
public class JSONFunctionThrowsModel extends JSONModel
{
	/**
	 * Le nom du throws.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La raison pouvant amener au throws.
	 */
	@SerializedName("description")
	public String description = "";
	
	public String toString()
	{
		return name;
	}
}
