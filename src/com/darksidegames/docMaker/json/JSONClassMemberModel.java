/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Représente un membre d'une classe présente dans la documentation.
 * 
 * @author winterskill
 */
public class JSONClassMemberModel extends JSONModel
{
	/**
	 * Le nom du membre.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La portée du membre.
	 */
	@SerializedName("scope")
	public JSONScope scope = JSONScope.PUBLIC;
	/**
	 * Le type du membre.
	 */
	@SerializedName("type")
	public String type;
	/**
	 * Le membre est-t-il une constante ?
	 */
	@SerializedName("isConst")
	public boolean isConst = false;
	/**
	 * Le membre est-t-il static ?
	 */
	@SerializedName("isStatic")
	public boolean isStatic = false;
	/**
	 * Le membre est-t-il un pointeur ?
	 */
	@SerializedName("pointer")
	public boolean pointer = false;
	/**
	 * Le membre est-t-il final ?
	 */
	@SerializedName("isFinal")
	public boolean isFinal = false;
	/**
	 * La description du membre.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La version minimal du code où le membre existe.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * Le membre est-t-il obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * Le nom de l'auteur du membre.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La liste des annotations présentes sur le membre.
	 */
	@SerializedName("annotations")
	public List<JSONAnnotationModel> annotations;
	
	public String toString()
	{
		return name;
	}
}
