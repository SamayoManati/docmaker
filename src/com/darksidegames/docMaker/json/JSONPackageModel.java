/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;


import java.util.List;

import com.darksidegames.docMaker.json.calypso.JSONCalypsoSuitcaseModel;
import com.google.gson.annotations.SerializedName;

/**
 * Représente un package dans la documentation.
 * 
 * @author winterskill
 */
public class JSONPackageModel extends JSONModel
{
	/**
	 * Le nom du package.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La description du package.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La version minimale du code dans laquelle le package est présent.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * La liste des fonctions présentes dans le package.
	 */
	@SerializedName("functions")
	public List<JSONFunctionModel> functions;
	/**
	 * La liste des classes présentes dans le package.
	 */
	@SerializedName("classes")
	public List<JSONClassModel> classes;
	/**
	 * La liste des interfaces présents dans le package.
	 */
	@SerializedName("interfaces")
	public List<JSONInterfaceModel> interfaces;
	/**
	 * La liste des variables présentes dans le package.
	 */
	@SerializedName("variables")
	public List<JSONVariableModel> variables;
	/**
	 * La liste des annotations présentes dans le package.
	 */
	@SerializedName("annotations")
	public List<JSONAnnotationElementModel> annotations;
	/**
	 * La liste des constantes de préprocesseur présentes dans le package.
	 */
	@SerializedName("preprocessorConstants")
	public List<JSONPreprocessorConstantModel> preprocessorConstants;
	/**
	 * La liste des suitcases calypso présentes dans le package.
	 * 
	 * ATTENTION! N'est utilisé que par la documentation en Calypso.
	 */
	@SerializedName("suitcases")
	public List<JSONCalypsoSuitcaseModel> suitcases;
	/**
	 * La liste des typedefs présents dans le package.
	 */
	@SerializedName("typedefs")
	public List<JSONTypedefModel> typedefs;
	/**
	 * S'agit-t-il d'un package importé par défaut ?
	 */
	@SerializedName("isLangPackage")
	public boolean isLangPackage = false;
	
	public String toString()
	{
		return name;
	}
}
