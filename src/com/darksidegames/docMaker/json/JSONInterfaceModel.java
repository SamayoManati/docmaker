/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * Représente un interface dans la documentation.
 * 
 * @author winterskill
 */
public class JSONInterfaceModel extends JSONModel
{
	/**
	 * Le nom de l'interface.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La liste des méthodes de l'interface.
	 */
	@SerializedName("methods")
	public List<JSONClassMethodModel> methods;
	/**
	 * La liste des membres de l'interface.
	 */
	@SerializedName("members")
	public List<JSONClassMemberModel> members;
	/**
	 * La description de l'interface.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La description courte de l'interface.
	 */
	@SerializedName("shortDescription")
	public String shortDescription = "";
	/**
	 * L'auteur de l'interface.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La version minimale du code dans laquelle on peut utiliser
	 * l'interface.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * L'interface est-t-il obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * Les annotations présentes sur l'interface.
	 */
	@SerializedName("annotations")
	public List<JSONAnnotationModel> annotations;
	
	public String toString()
	{
		return name;
	}
}
