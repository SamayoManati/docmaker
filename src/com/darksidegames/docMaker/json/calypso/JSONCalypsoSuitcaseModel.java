/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json.calypso;

import java.util.List;

import com.darksidegames.docMaker.json.JSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Représente une suitcase de calypso dans la documentation.
 * 
 * @author winterskill
 */
public class JSONCalypsoSuitcaseModel extends JSONModel
{
	/**
	 * Le nom.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La description courte.
	 */
	@SerializedName("shortDescription")
	public String shortDescription = "";
	/**
	 * La description.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * L'auteur de la suitcase.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La version minimale du code dans laquelle on peut utiliser la suitcase.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * La suitcase est-t-elle obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	/**
	 * La liste des clefs contenues dans la suitcase.
	 */
	@SerializedName("content")
	public List<JSONCalypsoSuitcaseKeyModel> content;
	
	public String toString()
	{
		return name;
	}
}
