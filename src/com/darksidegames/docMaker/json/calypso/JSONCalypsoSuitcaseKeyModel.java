/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json.calypso;

import com.darksidegames.docMaker.json.JSONModel;
import com.google.gson.annotations.SerializedName;

/**
 * Représente une clef contenue dans une suitcase calypso.
 * 
 * @author winterskill
 */
public class JSONCalypsoSuitcaseKeyModel extends JSONModel
{
	/**
	 * Le nom de la clef.
	 */
	@SerializedName("name")
	public String name;
	/**
	 * La valeur de la clef.
	 */
	@SerializedName("value")
	public String value = "";
	/**
	 * La description de la clef.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La version minimale du code dans laquelle on peut utiliser la clef.
	 */
	@SerializedName("since")
	public String since = "";
	/**
	 * La clef est-t-elle obsolète ?
	 */
	@SerializedName("deprecated")
	public boolean deprecated = false;
	
	public String toString()
	{
		return name;
	}
}
