/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.json;

import java.util.List;

import com.darksidegames.docMaker.config.Languages;

import com.google.gson.annotations.SerializedName;


/**
 * La classe qui contient le modèle de la structure du fichier JSON de source de doc.
 * 
 * @author winterskill
 */
public class JSONSourceModel extends JSONModel
{
	/**
	 * Le nom de la documentation.
	 */
	@SerializedName("name")
	public String name = "";
	/**
	 * La description de la documentation.
	 */
	@SerializedName("description")
	public String description = "";
	/**
	 * La version de la documentation.
	 */
	@SerializedName("version")
	public String version = "";
	/**
	 * L'auteur de la documentation.
	 */
	@SerializedName("author")
	public String author = "";
	/**
	 * La liste des packages présents dans la documentation.
	 */
	@SerializedName("packages")
	public List<JSONPackageModel> packages;
	
	/* META DONNEES */
	/**
	 * Le langage de programmation de la documentation.
	 */
	@SerializedName("$language")
	public Languages _languages;
	
	public String toString()
	{
		return name;
	}
}
