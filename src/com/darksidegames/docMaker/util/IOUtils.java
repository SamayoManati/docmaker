/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Cette classe abstraite contient des fonctions utilitaires sur la gestion de
 * fichiers.
 * Il est recommandé de l'utiliser avec des imports statiques.
 * 
 * @author winterskill
 *
 */
public abstract class IOUtils
{
	/**
	 * <p>Ecrit dans un fichier.</p>
	 * <p>Si le fichier n'existe pas, il sera créé automatiquement.</p>
	 * 
	 * @param filepath
	 * Le chemin du fichier.
	 * @param content
	 * Le contenu à écrire.
	 * @param append
	 * Si <code>true</code>, ajoute <code>content</code> à la fin du contenu a
	 * la fin du contenu actuel du fichier. Si <code>false</code>, écrase le
	 * contenu actuel du fichier.
	 */
	public static void fwrite(String filepath, String content, boolean append)
	{
		File file = new File(filepath);
		String contentToWrite = "";
		
		if (!file.exists()) try {
			file.createNewFile();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		if (append) {
			FileInputStream fis;
			
			try {
				fis = new FileInputStream(file);
				byte[] buffer = new byte[(int) file.length()];
				fis.read(buffer);
				contentToWrite += new String(buffer, "UTF-8");
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		contentToWrite += content;
		
		try {
			PrintWriter pw = new PrintWriter(filepath);
			pw.print(contentToWrite);
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return;
	}
	
	/**
	 * <p>Retourne le contenu d'un fichier.</p>
	 * <p>Le fichier doit exister.</p>
	 * 
	 * @param filepath
	 * Le chemin d'accès au fichier.
	 * @return Le contenu du fichier
	 * 
	 * @throws FileNotFoundException
	 * Quand le fichier n'existe pas.
	 */
	public static String fread(String filepath) throws FileNotFoundException
	{
		File file = new File(filepath);
		String retval = "";
		
		if (!file.exists())
			throw new FileNotFoundException("The file \"" + filepath + "\" does not exists");
		
		FileInputStream fis;
		try {
			fis = new FileInputStream(file);
			byte[] buffer = new byte[(int) file.length()];
			fis.read(buffer);
			retval += new String(buffer, "UTF-8");
			fis.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return retval;
	}
}
