/*
 * DocMaker
 * Copyright (C) 2018  Soni Tourret
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.darksidegames.docMaker;

import com.darksidegames.docMaker.config.AppearanceConfigUnit;
import com.darksidegames.docMaker.config.ExitCodes;
import com.darksidegames.docMaker.config.Languages;
import com.darksidegames.docMaker.doc.ObjectsFinder;
import com.darksidegames.docMaker.doc.ObjectsFinderObject;
import com.darksidegames.docMaker.doc.ObjectsFinderSearchOrder;
import com.darksidegames.docMaker.doc.OutputFile;
import com.darksidegames.docMaker.entryPoint.EntryPoint;
import com.darksidegames.docMaker.exceptions.MalformedDataException;
import com.darksidegames.docMaker.exceptions.NoObjectNameException;
import com.darksidegames.docMaker.exceptions.TermNotFoundException;
import com.darksidegames.docMaker.json.JSONSourceModel;
import static com.darksidegames.docMaker.util.StringUtils.isStringEmpty;

import com.google.gson.Gson;

import java.util.HashMap;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Cette classe représente une documentation générée à partir de fichier source
 * JSON.
 * 
 * Procédure :
 * 1. validate()
 * 2. [calcStatistics()]
 * 3. generate()
 * 
 * @author winterskill
 */
public class Documentation
{
	/**
	 * Le nombre de fichiers créés.
	 */
	protected int filesCreatedCount	= 0;
	/**
	 * Contient les données JSON utilisées pour générer la documentation.
	 */
	public JSONSourceModel sourceDatas;
	/**
	 * Le langage de programmation utilisé.
	 */
	protected Languages language;
	/**
	 * Contient les détails des types de contenu dans la doc.
	 */
	protected HashMap<String, Integer> contentTypeDetails;
	/**
	 * Les statistiques ont-t-elles déjà été calculées ?
	 */
	protected boolean contentTypeDetailsCalculated;
	/**
	 * Le chercheur d'objets de la documentation.
	 */
	protected ObjectsFinder objectsFinder;

	/**
	 * 
	 * @param datas
	 * Les données JSON sous forme de string. Mettez {@code null} pour
	 * signifier "documentation vide".
	 */
	public Documentation(String datas, Languages language)
	{
		DebugManager.print("com.darksidegames.docMaker:Documentation@Documentation", "new documentation created");
		
		this.language = language;
		this.contentTypeDetailsCalculated = false;
		this.contentTypeDetails = new HashMap<String, Integer>();
		this.objectsFinder = new ObjectsFinder(this.language);

		if (datas != null) {
			Gson gson = new Gson();
			this.sourceDatas = gson.fromJson(datas, JSONSourceModel.class);
		} else {
			this.sourceDatas = new JSONSourceModel();
		}
		this.refreshLangPackages();
	}
	
	public Documentation(String datas)
	{
		this(datas, new Gson().fromJson(datas, JSONSourceModel.class)._languages);
		// la ligne ci-dessus est si alambiquée parce que java me force à
		// utiliser le this() en 1er statement.
	}
	
	/**
	 * Retourne le langage de la documentation.
	 * 
	 * @return Le langage.
	 */
	public Languages getLanguage()
	{
		return this.language;
	}

	/**
	 * Actualise la liste des packages importés par défaut.
	 */
	public void refreshLangPackages()
	{
		this.objectsFinder.emptyLangPackages();
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).isLangPackage)
					this.objectsFinder.registerLangPackage(this.sourceDatas.packages.get(i).name);
			}
		}
	}

	/**
	 * Valide les données selon le modèle.
	 * 
	 * @throws MalformedDataException
	 * Si les données ne correspondent pas au modèle.
	 */
	public void validate() throws MalformedDataException
	{
		DebugManager.print("com.darksidegames.docMaker:Documentation@validate", "Validating...");
		if (this.sourceDatas.packages != null) {
			// si il y a des packages
			for (int packagesCount = 0; packagesCount < this.sourceDatas.packages.size(); packagesCount++) {
				// pour chaque package
				String packagesCountS = new StringBuilder().append("").append(packagesCount).toString();

				if (this.sourceDatas.packages.get(packagesCount).name == null) {
					// si "name" n'est pas renseigné
					throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].name : mandatory element");
				}

				if (this.sourceDatas.packages.get(packagesCount).functions != null) {
					// si il y a des fonctions
					for (int functionsCount = 0;
							functionsCount < this.sourceDatas.packages.get(packagesCount).functions.size();
							functionsCount++) {
						// pour chaque fonction
						String functionsCountS = new StringBuilder().append("").append(functionsCount).toString();

						if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).name == null) {
							// si "name" n'est pas renseigné
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].functions[" +
									functionsCountS + "].name : mandatory element");
						}

						if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).returnValue == null) {
							// si "returnValue" n'est pas renseigné
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].functions[" +
									functionsCountS + "].returnValue : mandatory element");
						}

						if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).args != null) {
							for (int argsCount = 0;
									argsCount < this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).args.size();
									argsCount++) {
								String argsCountS = new StringBuilder().append("").append(argsCount).toString();

								if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).args.get(argsCount).name == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].functions[" +
											functionsCountS + "].args[" + argsCountS + "].name : mandatory element");
								
								if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).args.get(argsCount).type == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].functions[" +
											functionsCountS + "].args[" + argsCountS + "].type : mandatory element");
							}
						}
						
						if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).annotations != null) {
							for (int annotationsCount = 0;
									annotationsCount < this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).annotations.size();
									annotationsCount++) {
								String annotationsCountS = new StringBuilder().append("").append(annotationsCount).toString();
								
								if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).annotations.get(annotationsCount).name == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].functions[" +
											functionsCountS + "].annotations[" + annotationsCountS + "].name : mandatory element");
							}
						}
						
						if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).throwsList != null) {
							for (int throwsListCount = 0;
									throwsListCount < this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).throwsList.size();
									throwsListCount++) {
								String throwsListCountS = new StringBuilder().append("").append(throwsListCount).toString();
								
								if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).throwsList.get(throwsListCount).name == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].functions[" +
											functionsCountS + "].throwsList[" + throwsListCountS + "].name : mandatory element");
							}
						}
						
						if ((this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).template)
								&& (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).template_args != null)) {
							for (int templateArgsCount = 0;
									templateArgsCount < this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount)
											.template_args.size();
									templateArgsCount++) {
								String templateArgsCountS = new StringBuilder().append("").append(templateArgsCount).toString();
								
								if (this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount)
										.template_args.get(templateArgsCount).name == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].functions[" +
										functionsCountS + "].template_args[" + templateArgsCountS + "].name : mandatory element");
							}
						}
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).classes != null) {
					// si il y a des classes
					for (int classesCount = 0;
							classesCount < this.sourceDatas.packages.get(packagesCount).classes.size();
							classesCount++) {
						String classesCountS = new StringBuilder().append("").append(classesCount).toString();
						
						if (this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).name == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].classes[" + classesCountS + "].name : mandatory element");
						
						if (this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).members != null) {
							// si il y a des membres dans la classe
							for (int membersCount = 0;
									membersCount < this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).members.size();
									membersCount++) {
								String membersCountS = new StringBuilder().append("").append(membersCount).toString();
								
								if (this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).members.get(membersCount).name == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].classes[" +
											classesCountS + "].members[" + membersCountS + "].name : mandatory element");
								
								if (this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).members.get(membersCount).type == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].classes[" +
											classesCountS + "].members[" + membersCountS + "].type : mandatory element");
							}
						}
						
						if (this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).methods != null) {
							// si il y a des méthodes dans la classe
							for (int methodsCount = 0;
									methodsCount < this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).methods.size();
									methodsCount++) {
								String methodsCountS = new StringBuilder().append("").append(methodsCount).toString();
								
								if (this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).methods.get(methodsCount).name == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].classes[" + classesCountS +
											"].methods[" + methodsCountS + "].name : mandatory element");
								
								if (this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).methods.get(methodsCount).returnValue == null)
									throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].classes[" + classesCountS +
											"].methods[" + methodsCountS + "].returnValue : mandatory element");
							}
						}
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).interfaces != null) {
					// si il y a des interfaces
					for (int interfacesCount = 0;
							interfacesCount < this.sourceDatas.packages.get(packagesCount).interfaces.size();
							interfacesCount++) {
						String interfacesCountS = new StringBuilder().append("").append(interfacesCount).toString();
						
						if (this.sourceDatas.packages.get(packagesCount).interfaces.get(interfacesCount).name == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].interfaces[" +
									interfacesCountS + "].name : mandatory element");
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).variables != null) {
					// si il y a des variables
					for (int variablesCount = 0;
							variablesCount < this.sourceDatas.packages.get(packagesCount).variables.size();
							variablesCount++) {
						String variablesCountS = new StringBuilder().append("").append(variablesCount).toString();
						
						if (this.sourceDatas.packages.get(packagesCount).variables.get(variablesCount).name == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].variables[" + variablesCountS +
									"].name : mandatory element");
						
						if (this.sourceDatas.packages.get(packagesCount).variables.get(variablesCount).type == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].variables[" + variablesCountS +
									"].type : mandatory element");
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).annotations != null) {
					// si il y a des annotations
					for (int annotationsCount = 0;
							annotationsCount < this.sourceDatas.packages.get(packagesCount).annotations.size();
							annotationsCount++) {
						String annotationsCountS = new StringBuilder().append("").append(annotationsCount).toString();
						
						if (this.sourceDatas.packages.get(packagesCount).annotations.get(annotationsCount).name == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].annotations[" + annotationsCountS +
									"].name : mandatory element");
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).preprocessorConstants != null) {
					// si il y a des constantes de préprocesseur
					for (int preprocessorConstantsCount = 0;
							preprocessorConstantsCount < this.sourceDatas.packages.get(packagesCount).preprocessorConstants.size();
							preprocessorConstantsCount++) {
						String preprocessorConstantsCountS = new StringBuilder().append("").append(preprocessorConstantsCount)
								.toString();
						
						if (this.sourceDatas.packages.get(packagesCount).preprocessorConstants.get(preprocessorConstantsCount).name == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].preprocessorConstants[" +
									preprocessorConstantsCountS + "].name : mandatory element");
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).typedefs != null) {
					// si il y a des typedefs
					for (int typedefsCount = 0;
							typedefsCount < this.sourceDatas.packages.get(packagesCount).typedefs.size();
							typedefsCount++) {
						String typedefsCountS = new StringBuilder().append("").append(typedefsCount).toString();
						
						if (this.sourceDatas.packages.get(packagesCount).typedefs.get(typedefsCount).name == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].typedefs[" + typedefsCountS +
									"].name : mandatory element");
						
						if (this.sourceDatas.packages.get(packagesCount).typedefs.get(typedefsCount).value == null)
							throw new MalformedDataException("ROOT.packages[" + packagesCountS + "].typedefs[" + typedefsCountS +
									"].type : mandatory element");
					}
				}
			}
		}
		
		/* PARTIES DE SCHEMA UNIQUEMENT POUR CERTAINS LANGAGES DE PROGRAMMATION */
		try {
			this.language.validate(this.sourceDatas);
		} catch (NullPointerException e) {
			DebugManager.print("com.darksidegames.docMaker:Documentation@validate", "The given language isn't recognized...", DebugManager.MESSAGE_ERROR);
			DebugManager.sendErrorMessage("Resource error", "The given language isn't recognized", true);
			EntryPoint.quitProgram(ExitCodes.DOCUMENTATION_GIVEN_LANGUAGE_DOES_NOT_EXISTS.getCode());
		}
		
		DebugManager.print("com.darksidegames.docMaker:Documentation@validate", "Documentation validated");
	}
	
	/**
	 * Calcule des statistiques du contenu de la documentation.
	 * 
	 * @param force
	 * Si TRUE, force à faire le calcul. Sinon, le calcul ne sera effectué que s'il n'a
	 * jamais été fait.
	 */
	public void calcStatistics(boolean force)
	{
		if (!this.contentTypeDetailsCalculated || force) {
			if (this.sourceDatas.packages != null) {
				// si il y a des packages
				for (int packagesCount = 0;
						packagesCount < this.sourceDatas.packages.size();
						packagesCount++) {
					if (this.contentTypeDetails.containsKey("packages")) {
						this.contentTypeDetails.put("packages",
								this.contentTypeDetails.get("packages") + 1);
					} else {
						this.contentTypeDetails.put("packages", 1);
					}
				
					if (this.sourceDatas.packages.get(packagesCount).functions != null) {
						// si il y a des fonctions
						for (int functionsCount = 0;
								functionsCount < this.sourceDatas.packages.get(packagesCount).functions.size();
								functionsCount++) {
							if (this.contentTypeDetails.containsKey("functions")) {
								this.contentTypeDetails.put("functions",
										this.contentTypeDetails.get("functions") + 1);
							} else {
								this.contentTypeDetails.put("functions", 1);
							}
						}
					}
				
					if (this.sourceDatas.packages.get(packagesCount).classes != null) {
						// si il y a des classes
						for (int classesCount = 0;
								classesCount < this.sourceDatas.packages.get(packagesCount).classes.size();
								classesCount++) {
							if (this.contentTypeDetails.containsKey("classes")) {
								this.contentTypeDetails.put("classes",
										this.contentTypeDetails.get("classes") + 1);
							} else {
								this.contentTypeDetails.put("classes", 1);
							}
						}
					}
				
					if (this.sourceDatas.packages.get(packagesCount).interfaces != null) {
						// si il y a des interfaces
						for (int interfacesCount = 0;
								interfacesCount < this.sourceDatas.packages.get(packagesCount).interfaces.size();
								interfacesCount++) {
							if (this.contentTypeDetails.containsKey("interfaces")) {
								this.contentTypeDetails.put("interfaces",
										this.contentTypeDetails.get("interfaces") + 1);
							} else {
								this.contentTypeDetails.put("interfaces", 1);
							}
						}
					}
				
					if (this.sourceDatas.packages.get(packagesCount).variables != null) {
						// si il y a des variables
						for (int variablesCount = 0;
								variablesCount < this.sourceDatas.packages.get(packagesCount).variables.size();
								variablesCount++) {
							if (this.contentTypeDetails.containsKey("variables")) {
								this.contentTypeDetails.put("variables",
										this.contentTypeDetails.get("variables") + 1);
							} else {
								this.contentTypeDetails.put("variables", 1);
							}
						}
					}
				
					if (this.sourceDatas.packages.get(packagesCount).annotations != null) {
						// si il y a des annotations
						for (int annotationsCount = 0;
								annotationsCount < this.sourceDatas.packages.get(packagesCount).annotations.size();
								annotationsCount++) {
							if (this.contentTypeDetails.containsKey("annotations")) {
								this.contentTypeDetails.put("annotations",
										this.contentTypeDetails.get("annotations") + 1);
							} else {
								this.contentTypeDetails.put("annotations", 1);
							}
						}
					}
				
					if (this.sourceDatas.packages.get(packagesCount).preprocessorConstants != null) {
						for (int preprocessorConstantsCount = 0;
								preprocessorConstantsCount < this.sourceDatas.packages.get(packagesCount).preprocessorConstants.size();
								preprocessorConstantsCount++) {
							if (this.contentTypeDetails.containsKey("preprocessorConstants")) {
								this.contentTypeDetails.put("preprocessorConstants",
										this.contentTypeDetails.get("preprocessorConstants") + 1);
							} else {
								this.contentTypeDetails.put("preprocessorConstants", 1);
							}
						}
					}
				
					if (this.sourceDatas.packages.get(packagesCount).typedefs != null) {
						for (int typedefsCount = 0;
								typedefsCount < this.sourceDatas.packages.get(packagesCount).typedefs.size();
								typedefsCount++) {
							if (this.contentTypeDetails.containsKey("typedefs")) {
								this.contentTypeDetails.put("typedefs",
									this.contentTypeDetails.get("typedefs") + 1);
							} else {
								this.contentTypeDetails.put("typedefs", 1);
							}
						}
					}
					
					/* PARTIES DE CONTENUS UNIQUEMENT POUR CERTAINS LANGAGES DE PROGRAMMATION */
					this.language.calcStatistics(this.sourceDatas, this.contentTypeDetails);
				}
			}
		
			this.contentTypeDetailsCalculated = true;
			
			DebugManager.print("com.darksidegames.docMaker:Documentation@calcStatistics", "Statistics calculated");
		} else
			DebugManager.print("com.darksidegames.docMaker:Documentation@calcStatistics",
					"Statistics already calculated", DebugManager.MESSAGE_WARNING);
	}
	
	/**
	 * Calcule des statistiques du contenu de la documentation.
	 * 
	 * ATTENTION! Le calcul ne sera fait qu'une seule fois par documentation.
	 */
	public void calcStatistics()
	{
		this.calcStatistics(false);
	}
	
	/**
	 * Retourne une HashMap contenant les statistiques du contenu de la documentation.
	 * Si la méthode calcStatistics n'a jamais été appelée, l'appelle puis retourne les
	 * statistiques.
	 * 
	 * @return Les statistiques, sous la forme type=nombre.
	 */
	public HashMap<String, Integer> getStatistics()
	{
		if (!this.contentTypeDetailsCalculated)
			this.calcStatistics();
		
		return this.contentTypeDetails;
	}
	
	/**
	 * Copie les fichiers et dossiers de base.
	 * Indispensable pour créer la base de la documentation générée.
	 * 
	 * @param path
	 * Le chemin où copier la base
	 * 
	 * @throws NullPointerException
	 * Si la source n'existe pas.
	 * @throws IOException
	 */
	protected void copyBaseFiles(String path) throws NullPointerException, IOException
	{
		// tkt bb ça marche
		// (avec des modifs, mais ça marche)
		// ((en plus, j'ai testé))
		
		final int bufferLength = 1024;
		
		File cssOutputDir = new File(path + "/css");
		if (!cssOutputDir.exists())
			cssOutputDir.mkdirs();
		File fontsOutputDir = new File(path + "/fonts");
		if (!fontsOutputDir.exists())
			fontsOutputDir.mkdirs();
		File jsOutputDir = new File(path + "/js");
		if (!jsOutputDir.exists())
			jsOutputDir.mkdirs();
		File elementsOutputDir = new File(path + "/elements");
		if (!elementsOutputDir.exists())
			elementsOutputDir.mkdirs();
		
		File cssDir = new File("./base/css");
		File[] cssFilesSource = cssDir.listFiles();
		if (cssFilesSource == null)
			throw new IOException("\"css\" entry is not a directory");
		for (int i = 0; i < cssFilesSource.length; i++) {
			if (cssFilesSource[i].isFile()) {
				InputStream is = null;
				OutputStream os = null;
				
				try {
					is = new FileInputStream(cssFilesSource[i]);
					os = new FileOutputStream(new File(path + "/css/" + cssFilesSource[i].getName()));
					byte[] buffer = new byte[bufferLength];
					int nbLectures = 0;
					
					while ((nbLectures = is.read(buffer)) > 0)
						os.write(buffer, 0, nbLectures);
				} finally {
					is.close();
					os.close();
				}
			}
		}
		
		File fontsDir = new File("./base/fonts");
		File[] fontsFilesSource = fontsDir.listFiles();
		if (fontsFilesSource == null)
			throw new IOException("\"fonts\" entry is not a directory");
		for (int i = 0; i < fontsFilesSource.length; i++) {
			if (fontsFilesSource[i].isFile()) {
				InputStream is = null;
				OutputStream os = null;
				
				try {
					is = new FileInputStream(fontsFilesSource[i]);
					os = new FileOutputStream(new File(path + "/fonts/" + fontsFilesSource[i].getName()));
					byte[] buffer = new byte[bufferLength];
					int nbLectures = 0;
					
					while ((nbLectures = is.read(buffer)) > 0)
						os.write(buffer, 0, nbLectures);
				} finally {
					is.close();
					os.close();
				}
			}
		}
		
		File jsDir = new File("./base/js");
		File[] jsFilesSource = jsDir.listFiles();
		if (jsFilesSource == null)
			throw new IOException("\"js\" entry is not a directory");
		for (int i = 0; i < jsFilesSource.length; i++) {
			if (jsFilesSource[i].isFile()) {
				InputStream is = null;
				OutputStream os = null;
				
				try {
					is = new FileInputStream(jsFilesSource[i]);
					os = new FileOutputStream(new File(path + "/js/" + jsFilesSource[i].getName()));
					byte[] buffer = new byte[bufferLength];
					int nbLectures = 0;
					
					while ((nbLectures = is.read(buffer)) > 0)
						os.write(buffer, 0, nbLectures);
				} finally {
					is.close();
					os.close();
				}
			}
		}
		
		DebugManager.print("com.darksidegames.docMaker:Documentation@copyBaseFiles", "Base files copied");
	}
	
	/**
	 * Génère les fichiers de la documentation.
	 * 
	 * @param path
	 * Le chemin vers le dossier où créer la documentation.
	 * @throws IOException
	 * Si le dossier racine de la doc est un fichier.
	 */
	public void generate(String path) throws IOException
	{
		File docRootDir = new File(path);
		if (!docRootDir.exists()) {
			docRootDir.mkdirs();
		} else {
			if (docRootDir.isFile()) {
				// si le dossier racine existe déjà mais que c'est un fichier (pas de bol, hein?)
				throw new IOException("documentation root dir is a file");
			}
		}
		
		this.copyBaseFiles(path);
		
		// remplissage du chercheur d'objets
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Filling objects finder");
		if (this.sourceDatas.packages != null) {
			for (int packagesCount = 0; packagesCount < this.sourceDatas.packages.size(); packagesCount++) {
				this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount).name, ObjectsFinderObject.PACKAGE, "");
				
				if (this.sourceDatas.packages.get(packagesCount).functions != null) {
					for (int functionsCount = 0; functionsCount < this.sourceDatas.packages.get(packagesCount).functions.size();
							functionsCount++) {
						this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount).functions.get(functionsCount).name,
								ObjectsFinderObject.FUNCTION, this.sourceDatas.packages.get(packagesCount).name);
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).classes != null) {
					for (int classesCount = 0; classesCount < this.sourceDatas.packages.get(packagesCount).classes.size();
							classesCount++) {
						this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount).classes.get(classesCount).name,
								ObjectsFinderObject.CLASS, this.sourceDatas.packages.get(packagesCount).name);
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).interfaces != null) {
					for (int interfacesCount = 0;
							interfacesCount < this.sourceDatas.packages.get(packagesCount).interfaces.size();
							interfacesCount++) {
						this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount).interfaces.get(interfacesCount).name,
								ObjectsFinderObject.INTERFACE, this.sourceDatas.packages.get(packagesCount).name);
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).variables != null) {
					for (int variablesCount = 0;
							variablesCount < this.sourceDatas.packages.get(packagesCount).variables.size();
							variablesCount++) {
						this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount).variables.get(variablesCount).name,
								ObjectsFinderObject.VARIABLE, this.sourceDatas.packages.get(packagesCount).name);
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).annotations != null) {
					for (int annotationsCount = 0;
							annotationsCount < this.sourceDatas.packages.get(packagesCount).annotations.size();
							annotationsCount++) {
						this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount).annotations.get(annotationsCount).name,
								ObjectsFinderObject.ANNOTATION, this.sourceDatas.packages.get(packagesCount).name);
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).preprocessorConstants != null) {
					for (int preprocessorConstantsCount = 0;
							preprocessorConstantsCount < this.sourceDatas.packages.get(packagesCount).preprocessorConstants.size();
							preprocessorConstantsCount++) {
						this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount)
								.preprocessorConstants.get(preprocessorConstantsCount).name,
								ObjectsFinderObject.PREPROCESSOR_CONSTANTS, this.sourceDatas.packages.get(packagesCount).name);
					}
				}
				
				if (this.sourceDatas.packages.get(packagesCount).typedefs != null) {
					for (int typedefsCount = 0; typedefsCount < this.sourceDatas.packages.get(packagesCount).typedefs.size();
							typedefsCount++) {
						this.objectsFinder.register(this.sourceDatas.packages.get(packagesCount).typedefs.get(typedefsCount).name,
								ObjectsFinderObject.TYPEDEF, this.sourceDatas.packages.get(packagesCount).name);
					}
				}
			}
		}
		this.language.fillObjectsFinder(this.objectsFinder, this.sourceDatas);
		
		// création de index.html
		String name = (this.sourceDatas.name != null) ? this.sourceDatas.name : "Documentation";
		String file_index = "<!DOCTYPE HTML><head><title>" + name + "</title>";
		HashMap<String, String> generalInfos = new HashMap<String, String>();
		
		file_index += "<meta charset=\"utf-8\"/>";
		file_index += "<link rel=stylesheet href=\"css/style.css\"/></head>";
		file_index += "<body>";
		file_index += AppearanceConfigUnit.getHeaderMenu(false);
		file_index += "<div class=\"page-header\"><h1>" + name + "</h1></div>";
		if (this.sourceDatas.description != null && !this.sourceDatas.description.equals(""))
			file_index += "<p>" + this.sourceDatas.description + "</p>";
		if (this.sourceDatas.version != null && !this.sourceDatas.version.equals("")) {
			try {
				generalInfos.put(LexiqueManager.getTerm("term.version", "output"), this.sourceDatas.version);
			} catch (TermNotFoundException e) {
				e.printStackTrace();
			}
		}
		if (this.sourceDatas.author != null && !this.sourceDatas.author.equals("")) {
			try {
				generalInfos.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.author);
			} catch (TermNotFoundException e) {
				e.printStackTrace();
			}
		}
		if (!generalInfos.isEmpty())
			file_index += AppearanceConfigUnit.generateTable(generalInfos, "");
		file_index += "</body></html>";
		new OutputFile(path + "/index.html", file_index);
		this.filesCreatedCount++;
		
		// création de packages.html
		String file_packages = "<!DOCTYPE HTML><html><head><title>Packages</title><meta charset=\"utf-8\"><link rel=stylesheet href=\"css/style.css\"/></head>";
		file_packages += "<body>";
		file_packages += AppearanceConfigUnit.getHeaderMenu(false);
		file_packages += "<div class=\"page-header\"><h1>Packages</h1></div>";
		if (this.sourceDatas.packages != null) {
			file_packages += "<ul>";
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				String shownDescr = "";
				
				if (this.sourceDatas.packages.get(i).description != null &&
						!this.sourceDatas.packages.get(i).description.equals("")) {
					shownDescr = " : ";
					if (this.sourceDatas.packages.get(i).description.length() < AppearanceConfigUnit.descriptionMaxLength)
						shownDescr += this.sourceDatas.packages.get(i).description;
					else {
						shownDescr += this.sourceDatas.packages.get(i).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
						shownDescr += "...";
					}
				}
				
				if (this.sourceDatas.packages.get(i).since != null && !this.sourceDatas.packages.get(i).since.equals("")
						&& this.sourceDatas.version != null && !this.sourceDatas.version.equals("")
						&& this.sourceDatas.packages.get(i).since.equals(this.sourceDatas.version))
					shownDescr += " " + AppearanceConfigUnit.getLabelNew();
				
				file_packages += "<li><a href=\"elements/" + this.sourceDatas.packages.get(i).name + ".html\">"
						+ this.sourceDatas.packages.get(i).name + "</a>" + shownDescr + "</li>";
			}
		}
		file_packages += "</body></html>";
		new OutputFile(path + "/packages.html", file_packages);
		this.filesCreatedCount++;
		
		// création du fichier deprecated.html
		String file_deprecated = "<!DOCTYPE HTML><html><head><title>Deprecated</title><meta charset=\"utf-8\"/>";
		file_deprecated += "<link rel=stylesheet href=\"css/style.css\"/></head><body>";
		file_deprecated += AppearanceConfigUnit.getHeaderMenu(false);
		file_deprecated += "<div class=\"page-header\"><h1>Deprecated</h1></div>";
		file_deprecated += "<ul>";
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).functions != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).functions.size(); j++) {
						if (this.sourceDatas.packages.get(i).functions.get(j).deprecated)
							file_deprecated += "<li><a href=\"elements/"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).functions.get(j).name
									+ ".html\">"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).functions.get(j).name
									+ "</a></li>";
					}
				}
				
				if (this.sourceDatas.packages.get(i).classes != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).classes.size(); j++) {
						if (this.sourceDatas.packages.get(i).classes.get(j).deprecated)
							file_deprecated += "<li><a href=\"elements/" + this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).classes.get(j).name + ".html\">"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).classes.get(j).name + "</a></li>";
					}
				}
				
				if (this.sourceDatas.packages.get(i).interfaces != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).interfaces.size(); j++) {
						if (this.sourceDatas.packages.get(i).classes.get(j).deprecated)
							file_deprecated += "<li><a href=\"elements/"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).interfaces.get(j).name
									+ ".html\">"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).interfaces.get(j).name
									+ "</a></li>";
					}
				}
				
				if (this.sourceDatas.packages.get(i).variables != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).variables.size(); j++) {
						if (this.sourceDatas.packages.get(i).variables.get(j).deprecated)
							file_deprecated += "<li><a href=\"elements/"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).variables.get(j).name
									+ ".html\">"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).variables.get(j).name
									+ "</a></li>";
					}
				}
				
				if (this.sourceDatas.packages.get(i).annotations != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).annotations.size(); j++) {
						if (this.sourceDatas.packages.get(i).annotations.get(j).deprecated)
							file_deprecated += "<li><a href=\"elements/"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).annotations.get(j).name
									+ ".html\">"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).annotations.get(j).name
									+ "</a></li>";
					}
				}
				
				if (this.sourceDatas.packages.get(i).preprocessorConstants != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).preprocessorConstants.size(); j++) {
						if (this.sourceDatas.packages.get(i).preprocessorConstants.get(j).deprecated)
							file_deprecated += "<li><a href=\"elements/"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name
									+ ".html\">"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name
									+ "</a></li>";
					}
				}
				
				if (this.sourceDatas.packages.get(i).typedefs != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).typedefs.size(); j++) {
						if (this.sourceDatas.packages.get(i).typedefs.get(j).deprecated)
							file_deprecated += "<li><a href=\"elements/"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).typedefs.get(j).name
									+ ".html\">"
									+ this.sourceDatas.packages.get(i).name
									+ this.language.getEndPackagesSeparator()
									+ this.sourceDatas.packages.get(i).typedefs.get(j).name
									+ "</a></li>";
					}
				}
			}
		}
		file_deprecated += this.language.generateDeprecatedOutputFile(this.sourceDatas);
		file_deprecated += "</ul>";
		file_deprecated += "</body></html>";
		new OutputFile(path + "/deprecated.html", file_deprecated);
		this.filesCreatedCount++;
		
		// création des fichiers de packages
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Creating packages files");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				HashMap<String, String> lst = new HashMap<String, String>();
				String file = "<!DOCTYPE HTML><html><head>";
				file += "<title>" + this.sourceDatas.packages.get(i).name + "</title>";
				file += "<meta charset=\"utf-8\"/>";
				file += "<link rel=stylesheet href=\"../css/style.css\"/></head>";
				try {
					file += "<body>"
							+ AppearanceConfigUnit.getHeaderMenu(true)
							+ "<div class=\"page-header\"><h1>"
							+ this.sourceDatas.packages.get(i).name
							+ " <small>"
							+ LexiqueManager.getTerm("term.package", "output")
							+ "</small></h1></div>";
				} catch (TermNotFoundException e) {
					e.printStackTrace();
				}
				if (!isStringEmpty(this.sourceDatas.packages.get(i).description))
					file += "<p>" + this.sourceDatas.packages.get(i).description + "</p>";
				if (!isStringEmpty(this.sourceDatas.packages.get(i).since)) {
					try {
						lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).since);
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
				}
				
				if (!lst.isEmpty()) {
					file += "<br/>"; // TODO vérifier si ça fout pas la merde
					try {
						file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>"; // TODO what the fuck que ça fait là?
				}
				
				try {
					file += "<br/><h2>" + LexiqueManager.getTerm("term.content", "output") + "</h2>";
				} catch (TermNotFoundException e) {
					e.printStackTrace();
				}
				if (this.sourceDatas.packages.get(i).functions != null) {
					HashMap<String, String> lstFnts = new HashMap<String, String>();
					int elementsCount = 0;
					for (int j = 0; j < this.sourceDatas.packages.get(i).functions.size(); j++) {
						elementsCount++;
						String val = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).shortDescription))
							val = this.sourceDatas.packages.get(i).functions.get(j).shortDescription;
						else if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).description)) {
							if (this.sourceDatas.packages.get(i).functions.get(j).description.length() < AppearanceConfigUnit.descriptionMaxLength)
								val = this.sourceDatas.packages.get(i).functions.get(j).description;
							else {
								val += this.sourceDatas.packages.get(i).functions.get(j).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
								val += "...";
							}
						}
						
						String newLbl = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).since)
								&& !isStringEmpty(this.sourceDatas.version)
								&& this.sourceDatas.packages.get(i).functions.get(j).since.equals(this.sourceDatas.version))
							newLbl = " " + AppearanceConfigUnit.getLabelNew();
						lstFnts.put("<code><a href=\""
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).functions.get(j).name
								+ ".html\">"
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).functions.get(j).name
								+ "</a></code>"
								+ newLbl, val);
					}
					try {
						String s = new StringBuilder().append("").append(elementsCount).toString();
						file += AppearanceConfigUnit.generateTable(lstFnts,
								LexiqueManager.getTerm("title.functions", "output") + " <span class=\"badge\">" + s + "</span>");
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>";
				}
				if (this.sourceDatas.packages.get(i).classes != null) {
					HashMap<String, String> lstCls = new HashMap<String, String>();
					int elementsCount = 0;
					for (int j = 0; j < this.sourceDatas.packages.get(i).classes.size(); j++) {
						elementsCount++;
						String val = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).shortDescription))
							val = this.sourceDatas.packages.get(i).classes.get(j).shortDescription;
						else if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).description)) {
							if (this.sourceDatas.packages.get(i).classes.get(j).description.length() < AppearanceConfigUnit.descriptionMaxLength)
								val = this.sourceDatas.packages.get(i).classes.get(j).description;
							else {
								val += this.sourceDatas.packages.get(i).classes.get(j).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
								val += "...";
							}
						}
						
						String lblNew = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).since)
								&& !isStringEmpty(this.sourceDatas.version)
								&& this.sourceDatas.packages.get(i).classes.get(j).since.equals(this.sourceDatas.version))
							lblNew = " " + AppearanceConfigUnit.getLabelNew();
						lstCls.put("<code><a href=\""
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).classes.get(j).name
								+ ".html\">"
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).classes.get(j).name
								+ "</a></code>"
								+ lblNew, val);
					}
					try {
						String s = new StringBuilder().append(elementsCount).toString();
						file += AppearanceConfigUnit.generateTable(lstCls,
								LexiqueManager.getTerm("title.classes", "output") + " <span class=\"badge\">" + s + "</span>");
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>";
				}

				if (this.sourceDatas.packages.get(i).interfaces != null) {
					HashMap<String, String> lstItfs = new HashMap<String, String>();
					int elementsCount = 0;
					for (int j = 0; j < this.sourceDatas.packages.get(i).interfaces.size(); j++) {
						elementsCount++;
						String val = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).shortDescription))
							val = this.sourceDatas.packages.get(i).interfaces.get(j).shortDescription;
						else if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).description)) {
							if (this.sourceDatas.packages.get(i).interfaces.get(j).description.length() < AppearanceConfigUnit.descriptionMaxLength)
								val = this.sourceDatas.packages.get(i).interfaces.get(j).description;
							else {
								val = this.sourceDatas.packages.get(i).interfaces.get(j).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
								val += "...";
							}
						}
						
						String lblNew = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).since)
								&& !isStringEmpty(this.sourceDatas.version)
								&& this.sourceDatas.packages.get(i).interfaces.get(j).since.equals(this.sourceDatas.version))
							lblNew = " " + AppearanceConfigUnit.getLabelNew();
						
						lstItfs.put("<code><a href=\""
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).interfaces.get(j).name
								+ ".html\">"
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).interfaces.get(j).name
								+ "</a></code>"
								+ lblNew, val);
					}
					try {
						String s = new StringBuilder().append("").append(elementsCount).toString();
						file += AppearanceConfigUnit.generateTable(lstItfs,
								LexiqueManager.getTerm("title.interfaces", "output") + " <span class=\"badge\">" + s + "</span>");
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>";
				}
				
				if (this.sourceDatas.packages.get(i).variables != null) {
					HashMap<String, String> lstVars = new HashMap<String, String>();
					int elementsCount = 0;
					for (int j = 0; j < this.sourceDatas.packages.get(i).variables.size(); j++) {
						elementsCount++;
						String val = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).variables.get(j).description)) {
							if (this.sourceDatas.packages.get(i).variables.get(j).description.length() < AppearanceConfigUnit.descriptionMaxLength)
								val = this.sourceDatas.packages.get(i).variables.get(j).description;
							else {
								val = this.sourceDatas.packages.get(i).variables.get(j).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
								val += "...";
							}
						}
						
						String lblNew = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).variables.get(j).since)
								&& !isStringEmpty(this.sourceDatas.version)
								&& this.sourceDatas.packages.get(i).variables.get(j).since.equals(this.sourceDatas.version))
							lblNew = " " + AppearanceConfigUnit.getLabelNew();
						
						lstVars.put("<code><a href=\""
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).variables.get(j).name
								+ ".html\">"
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).variables.get(j).name
								+ "</a></code>"
								+ lblNew, val);
					}
					try {
						String s = new StringBuilder().append("").append(elementsCount).toString();
						file += AppearanceConfigUnit.generateTable(lstVars,
								LexiqueManager.getTerm("title.variables", "output") + " <span class=\"badge\">" + s + "</span>");
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>";
				}
				
				if (this.sourceDatas.packages.get(i).annotations != null) {
					HashMap<String, String> lstAnnots = new HashMap<String, String>();
					int elementsCount = 0;
					for (int j = 0; j < this.sourceDatas.packages.get(i).annotations.size(); j++) {
						elementsCount++;
						String val = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).shortDescription))
							val = this.sourceDatas.packages.get(i).annotations.get(j).shortDescription;
						else if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).description)) {
							if (this.sourceDatas.packages.get(i).annotations.get(j).description.length() < AppearanceConfigUnit.descriptionMaxLength)
								val = this.sourceDatas.packages.get(i).annotations.get(j).description;
							else {
								val = this.sourceDatas.packages.get(i).annotations.get(j).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
								val += "...";
							}
						}
						
						String lblNew = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).since)
								&& !isStringEmpty(this.sourceDatas.version)
								&& this.sourceDatas.packages.get(i).annotations.get(j).since.equals(this.sourceDatas.version))
							lblNew = " " + AppearanceConfigUnit.getLabelNew();
						
						lstAnnots.put("<code><a href=\""
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).annotations.get(j).name
								+ ".html\">"
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).annotations.get(j).name
								+ "</a></code>"
								+ lblNew, val);
					}
					try {
						String s = new StringBuilder().append("").append(elementsCount).toString();
						file += AppearanceConfigUnit.generateTable(lstAnnots,
								LexiqueManager.getTerm("title.annotations", "output") + " <span class=\"badge\">" + s + "</span>");
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>";
				}
				
				if (this.sourceDatas.packages.get(i).preprocessorConstants != null) {
					HashMap<String, String> lstPreprocs = new HashMap<String, String>();
					int elementsCount = 0;
					for (int j = 0; j < this.sourceDatas.packages.get(i).preprocessorConstants.size(); j++) {
						elementsCount++;
						String val = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).preprocessorConstants.get(j).description)) {
							if (this.sourceDatas.packages.get(i).preprocessorConstants.get(j).description.length() < AppearanceConfigUnit.descriptionMaxLength)
								val = this.sourceDatas.packages.get(i).preprocessorConstants.get(j).description;
							else {
								val = this.sourceDatas.packages.get(i).preprocessorConstants.get(j).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
								val += "...";
							}
						}
						
						String lblNew = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).preprocessorConstants.get(j).since)
								&& !isStringEmpty(this.sourceDatas.version)
								&& this.sourceDatas.packages.get(i).preprocessorConstants.get(j).since.equals(this.sourceDatas.version))
							lblNew = " " + AppearanceConfigUnit.getLabelNew();
						
						lstPreprocs.put("<code><a href=\""
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name
								+ ".html\">"
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name
								+ "</a></code>", val);
					}
					try {
						String s = new StringBuilder().append("").append(elementsCount).toString();
						file += AppearanceConfigUnit.generateTable(lstPreprocs,
								LexiqueManager.getTerm("title.preprocessorConstants", "output") + " <span class=\"badge\">" + s + "</span>");
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>";
				}
				
				if (this.sourceDatas.packages.get(i).typedefs != null) {
					HashMap<String, String> lstTdfs = new HashMap<String, String>();
					int elementsCount = 0;
					for (int j = 0; j < this.sourceDatas.packages.get(i).typedefs.size(); j++) {
						elementsCount++;
						String val = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).typedefs.get(j).description)) {
							if (this.sourceDatas.packages.get(i).typedefs.get(j).description.length() < AppearanceConfigUnit.descriptionMaxLength)
								val = this.sourceDatas.packages.get(i).typedefs.get(j).description;
							else {
								val = this.sourceDatas.packages.get(i).typedefs.get(j).description.substring(0, AppearanceConfigUnit.descriptionMaxLength);
								val += "...";
							}
						}
						
						String lblNew = "";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).typedefs.get(j).since)
								&& !isStringEmpty(this.sourceDatas.version)
								&& this.sourceDatas.packages.get(i).typedefs.get(j).since.equals(this.sourceDatas.version))
							lblNew = " " + AppearanceConfigUnit.getLabelNew();
						
						lstTdfs.put("<code><a href=\""
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).typedefs.get(j).name
								+ ".html\">"
								+ this.sourceDatas.packages.get(i).name
								+ this.language.getEndPackagesSeparator()
								+ this.sourceDatas.packages.get(i).typedefs.get(j).name
								+ "</a></code>", val);
					}
					try {
						String s = new StringBuilder().append("").append(elementsCount).toString();
						file += AppearanceConfigUnit.generateTable(lstTdfs,
								LexiqueManager.getTerm("title.typedefs", "output") + " <span class=\"badge\">" + s + "</span>");
					} catch (TermNotFoundException e) {
						e.printStackTrace();
					}
					file += "<br/>";
				}
				
				file += this.language.generatePackageOutputFile(this.sourceDatas.packages.get(i),
						this.sourceDatas.version);
				
				file += "</body></html>";
				new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + ".html", file);
				this.filesCreatedCount++;
			}
		}
		
		// création des fichiers de fonctions
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate",
				"Creating functions files");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).functions != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).functions.size(); j++) {
						String file = "<!DOCTYPE HTML><html><head><title>";
						file += this.sourceDatas.packages.get(i).functions.get(j).name;
						file += "</title><meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name,
									this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).functions.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>";
						file += this.sourceDatas.packages.get(i).functions.get(j).name;
						file += "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).functions.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).shortDescription))
							file += "<p>" + this.sourceDatas.packages.get(i).functions.get(j).shortDescription + "</p>";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).functions.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						try {
							lst.put(LexiqueManager.getTerm("term.returnValue", "output"),
									this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).functions.get(j).returnValue,
											ObjectsFinderSearchOrder.STD, true, this.sourceDatas.packages.get(i).name));
						} catch (TermNotFoundException e) {
							e.printStackTrace();
						} catch (NoObjectNameException e) {
							e.printStackTrace();
							try {
								lst.put(LexiqueManager.getTerm("term.returnValue", "output"), "<code>" + this.sourceDatas.packages.get(i).functions.get(j).returnValue + "</code>");
							} catch (TermNotFoundException e1) {
								e1.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).functions.get(j).ref) {
							try {
								lst.put(LexiqueManager.getTerm("term.returnRef", "output"),
										LexiqueManager.getTerm("term.yes", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).functions.get(j).pointer) {
							try {
								lst.put(LexiqueManager.getTerm("term.returnPointer", "output"),
										LexiqueManager.getTerm("term.yes", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"),
										this.sourceDatas.packages.get(i).functions.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"),
										this.sourceDatas.packages.get(i).functions.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).functions.get(j).annotations != null) {
							String v = "";
							for (int k = 0;
									k < this.sourceDatas.packages.get(i).functions.get(j).annotations.size();
									k++) {
								if (this.sourceDatas.packages.get(i).functions.get(j).annotations.get(k).args == null) {
									try {
										v += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).functions.get(j).annotations.get(k).name, ObjectsFinderSearchOrder.ANNOTATION, true, this.sourceDatas.packages.get(i).name);
									} catch (NoObjectNameException e) {
										e.printStackTrace();
									}
									v += "<br/>";
								} else {
									String asm = "<code>";
									try {
										asm += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).functions.get(j).annotations.get(k).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
									} catch (NoObjectNameException e) {
										e.printStackTrace();
									}
									asm += " ";
									for (int l = 0;
											l < this.sourceDatas.packages.get(i).functions.get(j).annotations.get(k).args.size();
											l++) {
										try {
											asm += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).functions.get(j).annotations.get(k).args.get(l),
													ObjectsFinderSearchOrder.STD, false, this.sourceDatas.packages.get(i).name);
										} catch (NoObjectNameException e) {
											e.printStackTrace();
										}
										if (l != this.sourceDatas.packages.get(i).functions.get(j).annotations.get(k).args.size() - 1)
											asm += ", ";
									}
									asm += "</code><br/>";
									
									v += asm;
								}
							}
							try {
								lst.put(LexiqueManager.getTerm("term.annotations", "output"), v);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
							
							// TODO wallah si ça marche je suis un boss
						}
						if (this.sourceDatas.packages.get(i).functions.get(j).throwsList != null) {
							String val = "";
							for (int k = 0; k < this.sourceDatas.packages.get(i).functions.get(j).throwsList.size(); k++) {
								try {
									val += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).functions.get(j).throwsList.get(k).name, ObjectsFinderSearchOrder.THROW, true, this.sourceDatas.packages.get(i).name);
								} catch (NoObjectNameException e) {
									e.printStackTrace();
								}
								if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).throwsList.get(k).description))
									val += " : "
											+ this.sourceDatas.packages.get(i).functions.get(j).throwsList.get(k).description;
							}
							try {
								lst.put(LexiqueManager.getTerm("term.exceptions", "output"), val);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).functions.get(j).template) {
							try {
								lst.put(LexiqueManager.getTerm("term.template", "output"),
										LexiqueManager.getTerm("term.yes", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst,
										LexiqueManager.getTerm("title.informations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (this.sourceDatas.packages.get(i).functions.get(j).args != null) {
							HashMap<String, String> lstArgs = new HashMap<String, String>();
							
							for (int k = 0;
									k < this.sourceDatas.packages.get(i).functions.get(j).args.size();
									k++) {
								String val = "";
								String key = "";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).args.get(k).description))
									val += this.sourceDatas.packages.get(i).functions.get(j).args.get(k).description;
								
								key = this.language.generateFunctionArg(this.sourceDatas.packages.get(i).functions.get(j).args.get(k),
										this.objectsFinder, this.sourceDatas.packages.get(i).name);
								lstArgs.put(key, val);
							}
							
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lstArgs,
										LexiqueManager.getTerm("title.arguments", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (this.sourceDatas.packages.get(i).functions.get(j).template) {
							if (this.sourceDatas.packages.get(i).functions.get(j).template_args != null) {
								HashMap<String, String> lstArgs = new HashMap<String, String>();
								
								for (int k = 0;
										k < this.sourceDatas.packages.get(i).functions.get(j).template_args.size();
										k++) {
									String val = "";
									String key = "";
									
									if (!isStringEmpty(this.sourceDatas.packages.get(i).functions.get(j).template_args.get(k).description))
										val += "<p>"
												+ this.sourceDatas.packages.get(i).functions.get(j).template_args.get(k).description
												+ "</p>";
									
									key = this.language.generateFunctionTemplateArg(this.sourceDatas.packages.get(i).functions.get(j).template_args.get(k), this.objectsFinder);
									
									lstArgs.put(key, val);
								}
								
								file += "<br/>";
								try {
									file += AppearanceConfigUnit.generateTable(lstArgs,
											LexiqueManager.getTerm("title.templateArguments", "output"));
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).functions.get(j).name + ".html",
								file);
						this.filesCreatedCount++;
					}
				}
			}
		}
		
		// création des fichiers de classe
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Creating classes files");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).classes != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).classes.size(); j++) {
						String file = "<!DOCTYPE HTML><html><head><title>";
						file += this.sourceDatas.packages.get(i).classes.get(j).name;
						file += "</title><meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).classes.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>";
						file += this.sourceDatas.packages.get(i).classes.get(j).name;
						file += "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).classes.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (this.sourceDatas.packages.get(i).classes.get(j).isAbstract) try {
							file += AppearanceConfigUnit.generateLabel(LexiqueManager.getTerm("term.abstract.f", "output"));
						} catch (TermNotFoundException e1) {
							e1.printStackTrace();
						}
						if (this.sourceDatas.packages.get(i).classes.get(j).isFinal) try {
							file += AppearanceConfigUnit.generateLabel(LexiqueManager.getTerm("term.final.f", "output"));
						} catch (TermNotFoundException e1) {
							e1.printStackTrace();
						}
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).shortDescription))
							file += "<p>" + this.sourceDatas.packages.get(i).classes.get(j).shortDescription + "</p>";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).classes.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						if (this.sourceDatas.packages.get(i).classes.get(j).isAbstract) {
							try {
								lst.put(LexiqueManager.getTerm("term.abstract.f", "output"), LexiqueManager.getTerm("term.yes", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).classes.get(j).isFinal) {
							try {
								lst.put(LexiqueManager.getTerm("term.final.f", "output"), LexiqueManager.getTerm("term.yes", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).classes.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).classes.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).classes.get(j).extendsList != null) {
							String val = "";
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).classes.get(j).extendsList.size(); k++) {
								try {
									val += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).extendsList.get(k), ObjectsFinderSearchOrder.EXTEND, true, this.sourceDatas.packages.get(i).name);
									val += "<br/>";
								} catch (NoObjectNameException e) {
									e.printStackTrace();
									val += "<code>" + this.sourceDatas.packages.get(i).classes.get(j).extendsList.get(k) + "</code><br/>";
								}
							}
							
							try {
								lst.put(LexiqueManager.getTerm("term.extends", "output"), val);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).classes.get(j).implementsList != null) {
							String val = "";
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).classes.get(j).implementsList.size(); k++) {
								try {
									val += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).implementsList.get(k), ObjectsFinderSearchOrder.IMPLEMENTATION, true, this.sourceDatas.packages.get(i).name);
									val += "<br/>";
								} catch (NoObjectNameException e) {
									e.printStackTrace();
									val += "<code>" + this.sourceDatas.packages.get(i).classes.get(j).implementsList.get(k) + "</code><br/>";
								}
							}
							
							try {
								lst.put(LexiqueManager.getTerm("term.implements", "output"), val);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).classes.get(j).annotations != null) {
							String val = "";
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).classes.get(j).annotations.size(); k++) {
								String val2 = "<code>";
								try {
									val2 += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).annotations.get(k).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
								} catch (NoObjectNameException e) {
									e.printStackTrace();
								}
								
								if (this.sourceDatas.packages.get(i).classes.get(j).annotations.get(k).args != null) {
									for (int l = 0; l < this.sourceDatas.packages.get(i).classes.get(j).annotations.get(k).args.size(); l++) {
										if (l != 0)
											val2 += ",";
										val2 += " " + this.sourceDatas.packages.get(i).classes.get(j).annotations.get(k).args.get(l);
									}
								}
								
								val2 += "</code><br/>";
								val += val2;
							}
							
							try {
								lst.put(LexiqueManager.getTerm("term.annotations", "output"), val);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (this.sourceDatas.packages.get(i).classes.get(j).members != null) {
							int elementsCount = 0;
							HashMap<String, String> membersHashMap = new HashMap<String, String>();
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).classes.get(j).members.size(); k++) {
								int modifiersCount = 0;
								String val = "";
								String key = "<code>";
								elementsCount++;
								
								if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).scope != null)
									key += this.sourceDatas.packages.get(i).classes.get(j).members.get(k).scope.getValue();
								else
									key += this.language.getDefaultScope().getValue();
								key += " ";
								
								if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).isStatic) {
									if (modifiersCount > 0)
										key += " ";
									key += "static";
									elementsCount++;
								}
								
								if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).isFinal) {
									if (modifiersCount > 0)
										key += " ";
									key += "final";
									elementsCount++;
								}
								
								if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).isConst) {
									if (modifiersCount > 0)
										key += " ";
									key += "const";
									elementsCount++;
								}
								
								if (modifiersCount > 0)
									key += " ";
								try {
									key += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).members.get(k).type, ObjectsFinderSearchOrder.TYPE, false, this.sourceDatas.packages.get(i).name);
								} catch (NoObjectNameException e) {
									e.printStackTrace();
									key += this.sourceDatas.packages.get(i).classes.get(j).members.get(k).type;
								}
								modifiersCount++;
								
								if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).pointer) {
									if (modifiersCount > 0)
										key += " ";
									key += "*";
									// pas de modifiersCount++ car on veut que le * soit collé au nom
								}
								
								if (modifiersCount > 0)
									key += " ";
								key += this.sourceDatas.packages.get(i).classes.get(j).members.get(k).name;
								
								key += "</code>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).members.get(k).since)
										&& !isStringEmpty(this.sourceDatas.version)
										&& this.sourceDatas.packages.get(i).classes.get(j).members.get(k).since.equals(this.sourceDatas.version))
									val += AppearanceConfigUnit.getLabelNew() + "<br/>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).members.get(k).description))
									val += this.sourceDatas.packages.get(i).classes.get(j).members.get(k).description;
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).members.get(k).since)) {
									try {
										val += "<br/><b>";
										val += LexiqueManager.getTerm("term.versionMin", "output");
										// TODO verifier ce terme
										val += " : </b>";
										val += this.sourceDatas.packages.get(i).classes.get(j).members.get(k).since;
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).members.get(k).author)) {
									try {
										val += "<br/><b>";
										val += LexiqueManager.getTerm("term.author", "output");
										val += " : </b>";
										val += this.sourceDatas.packages.get(i).classes.get(j).members.get(k).author;
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).deprecated)
									val += AppearanceConfigUnit.getDeprecatedWarning();
								
								if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).annotations != null) {
									val += "<br/><b>";
									try {
										val += LexiqueManager.getTerm("term.annotations", "output");
										// TODO vérifier ce terme
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
									val += " : </b><br/>";
									
									for (int l = 0; l < this.sourceDatas.packages.get(i).classes.get(j).members.get(k).annotations.size(); l++) {
										String v = "<code>";
										try {
											v += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).members.get(k).annotations.get(l).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
										} catch (NoObjectNameException e) {
											e.printStackTrace();
										}
										if (this.sourceDatas.packages.get(i).classes.get(j).members.get(k).annotations.get(l).args != null) {
											for (int m = 0; m < this.sourceDatas.packages.get(i).classes.get(j).members.get(k).annotations.get(l).args.size(); m++) {
												if (m != 1)
													v += ",";
												v += " ";
												v += this.sourceDatas.packages.get(i).classes.get(j).members.get(k).annotations.get(l).args.get(m);
											}
										}
										v += "</code><br/>";
										val += v;
									}
								}
								
								membersHashMap.put(key, val);
							}
							
							String elementsCountS = new StringBuilder().append("").append(elementsCount).toString();
							
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(membersHashMap, LexiqueManager.getTerm("title.members", "output") + " <span class=\"badge\">" + elementsCountS + "</span>");
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
							// TODO vérifier ce terme
						}
						
						if (this.sourceDatas.packages.get(i).classes.get(j).methods != null) {
							int constructorsElementsCount = 0;
							int destructorsElementsCount = 0;
							int elementsCount = 0;
							HashMap<String, String> constructorsHashMap = new HashMap<String, String>();
							HashMap<String, String> destructorsHashMap = new HashMap<String, String>();
							HashMap<String, String> elementsHashMap = new HashMap<String, String>();
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).classes.get(j).methods.size(); k++) {
								String val = "";
								String key = "<code><a href=\""
										+ this.sourceDatas.packages.get(i).name
										+ "."
										+ this.sourceDatas.packages.get(i).classes.get(j).name
										+ "."
										+ this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).name
										+ ".html\">"
										+ this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).name;
									
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).shortDescription))
									val += this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).shortDescription;
								else if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).description)) {
									if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).description.length() < AppearanceConfigUnit.descriptionMaxLength)
										val += this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).description;
									else
										val += this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).description.substring(0, AppearanceConfigUnit.descriptionMaxLength) + "...";
								}
								key += "</a></code>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).since)
										&& !isStringEmpty(this.sourceDatas.version)
										&& this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).since.equals(this.sourceDatas.version))
									key += " " + AppearanceConfigUnit.getLabelNew();
								
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isConstructor) {
									constructorsElementsCount++;
									constructorsHashMap.put(key, val);
								} else if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isDestructor) {
									destructorsElementsCount++;
									destructorsHashMap.put(key, val);
								} else {
									elementsCount++;
									elementsHashMap.put(key, val);
								}
							}
							
							final String constructorsElementsCountS = new StringBuilder().append("").append(constructorsElementsCount).toString();
							final String destructorsElementsCountS = new StringBuilder().append("").append(destructorsElementsCount).toString();
							final String elementsCountS = new StringBuilder().append("").append(elementsCount).toString();
							
							if (!constructorsHashMap.isEmpty()) {
								file += "<br/>";
								try {
									file += AppearanceConfigUnit.generateTable(constructorsHashMap, LexiqueManager.getTerm("title.constructors", "output") + " <span class=\"badge\">" + constructorsElementsCountS + "</span>");
									// TODO vérifier le terme.
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
							}
							if (!destructorsHashMap.isEmpty()) {
								file += "<br/>";
								try {
									file += AppearanceConfigUnit.generateTable(destructorsHashMap, LexiqueManager.getTerm("title.destructors", "output") + " <span class=\"badge\">" + destructorsElementsCountS + "</span>");
									// TODO vérifier le terme.
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
							}
							if (!elementsHashMap.isEmpty()) {
								file += "<br/>";
								try {
									file += AppearanceConfigUnit.generateTable(elementsHashMap, LexiqueManager.getTerm("title.methods", "output") + " <span class=\"badge\">" + elementsCountS + "</span>");
									// TODO vérifier le terme.
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).classes.get(j).name + ".html", file);
						this.filesCreatedCount++;
					}
				}
			}
		}
		
		// création des fichiers de méthodes
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Creating methods files");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).classes != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).classes.size(); j++) {
						if (this.sourceDatas.packages.get(i).classes.get(j).methods != null) {
							for (int k = 0; k < this.sourceDatas.packages.get(i).classes.get(j).methods.size(); k++) {
								String file = "<!DOCTYPE HTML><html><head>";
								file += "<title>" + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).name + "</title>";
								file += "<meta charset=\"utf-8\"/>";
								file += "<link rel=stylesheet href=\"../css/style.css\"/>";
								file += "</head><body>";
								file += AppearanceConfigUnit.getHeaderMenu(true);
								
								String[][] bcInfos = {
										{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
										{this.sourceDatas.packages.get(i).classes.get(j).name, this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).classes.get(j).name + ".html"},
										{this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).name, ""}
								};
								file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
								
								file += "<div class=\"page-header\"><h1>" + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).name + "</h1></div>";
								
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).deprecated)
									file += AppearanceConfigUnit.getDeprecatedWarning();
								
								String labels = "";
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isConstructor) {
									try {
										labels += AppearanceConfigUnit.generateLabel(LexiqueManager.getTerm("term.constructor", "output")) + " ";
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isDestructor) {
									try {
										labels += AppearanceConfigUnit.generateLabel(LexiqueManager.getTerm("term.destructor", "output")) + " ";
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (!isStringEmpty(labels)) {
									labels = "<p>" + labels + "</p>";
									file += labels;
								}
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).shortDescription))
									file += "<p>" + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).shortDescription + "</p>";
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).description))
									file += "<p>" + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).description + "</p>";
								
								HashMap<String, String> lst = new HashMap<String, String>();
								if (!this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isConstructor &&
										!this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isDestructor) {
									try {
										lst.put(LexiqueManager.getTerm("term.returnValue", "output"), this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).returnValue, ObjectsFinderSearchOrder.STD, true, this.sourceDatas.packages.get(i).name));
									} catch (TermNotFoundException e1) {
										e1.printStackTrace();
									} catch (NoObjectNameException e2) {
										e2.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).ref) {
									try {
										lst.put(LexiqueManager.getTerm("term.reference", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).pointer) {
									try {
										lst.put(LexiqueManager.getTerm("term.pointer", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								try {
									lst.put(LexiqueManager.getTerm("term.scope", "output"), "<code>" + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).scope.getValue() + "</code>");
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).since)) {
									try {
										lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).since);
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).author)) {
									try {
										lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).author);
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).annotations != null) {
									String val = "";
									for (int l = 0; l < this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).annotations.size(); l++) {
										String val2 = "<code>";
										try {
											val2 += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).annotations.get(l).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
										} catch (NoObjectNameException e) {
											e.printStackTrace();
										}
										
										if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).annotations.get(l).args != null) {
											for (int m = 0; m < this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).annotations.get(l).args.size(); m++) {
												if (m != 0)
													val2 += ",";
												val2 += " " + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).annotations.get(l).args.get(m);
											}
										}
										
										val2 += "</code><br/>";
										val += val2;
									}
									
									try {
										lst.put(LexiqueManager.getTerm("term.annotations", "output"), val);
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).throwsList != null) {
									String val = "";
									for (int l = 0; l < this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).throwsList.size(); l++) {
										try {
											val += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).throwsList.get(l).name, ObjectsFinderSearchOrder.THROW, true, this.sourceDatas.packages.get(i).name);
										} catch (NoObjectNameException e) {
											e.printStackTrace();
										}
										
										if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).throwsList.get(l).description))
											val += " : " + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).throwsList.get(l).description;
										val += "<br/>";
									}
									
									try {
										lst.put(LexiqueManager.getTerm("term.exceptions", "output"), val);
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isStatic) {
									try {
										lst.put(LexiqueManager.getTerm("term.static", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).isAbstract) {
									try {
										lst.put(LexiqueManager.getTerm("term.abstract.f", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								
								if (!lst.isEmpty()) {
									file += "<br/>";
									try {
										file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}

								if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args != null) {
									HashMap<String, String> argsmap = new HashMap<String, String>();
									for (int l = 0; l < this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.size(); l++) {
										int keyModifiersCount = 0;
										String val = "";
										String key = "<code>";

										if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).description)) {
											val += this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).description;
										}

										try {
											key += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).type, ObjectsFinderSearchOrder.TYPE, false, this.sourceDatas.packages.get(i).name);
											key += " ";
										} catch (NoObjectNameException e) {
											e.printStackTrace();
											key += this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).type;
											key += " ";
										}

										if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).isConst) {
											if (keyModifiersCount != 0)
												key += " ";
											key += "const";
											keyModifiersCount++;
										}

										if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).ref) {
											key += "&";
											keyModifiersCount++;
										}

										if (this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).pointer) {
											key += "*";
											keyModifiersCount++;
										}

										if (keyModifiersCount != 0)
											key += " ";
										key += this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).name;

										if (!isStringEmpty(this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).defaultValue)) {
											if (keyModifiersCount != 0)
												key += " = ";
											else
												key += "= ";
											key += this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).args.get(l).defaultValue;
											keyModifiersCount++;
										}

										key += "</code>";
										argsmap.put(key, val);
									}

									file += "<br/>";
									try {
										file += AppearanceConfigUnit.generateTable(argsmap, LexiqueManager.getTerm("title.arguments", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								
								file += "</body></html>";
								new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).classes.get(j).name + "." + this.sourceDatas.packages.get(i).classes.get(j).methods.get(k).name + ".html", file);
								this.filesCreatedCount++;
							}
						}
					}
				}

				// création des fichiers d'interfaces
				DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Generating methods files");
				if (this.sourceDatas.packages.get(i).interfaces != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).interfaces.size(); j++) {
						String file = "<!DOCTYPE HTML>";
						file += "<html><head>";
						file += "<title>" + this.sourceDatas.packages.get(i).interfaces.get(j).name + "</title>";
						file += "<meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).interfaces.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>" + this.sourceDatas.packages.get(i).interfaces.get(j).name + "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).interfaces.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).shortDescription))
							file += "<p>" + this.sourceDatas.packages.get(i).interfaces.get(j).shortDescription + "</p>";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).interfaces.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).interfaces.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).interfaces.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).interfaces.get(j).annotations != null) {
							String val = "";
							for (int k = 0; k < this.sourceDatas.packages.get(i).interfaces.get(j).annotations.size(); k++) {
								String val2 = "<code>";
								try {
									val2 += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).interfaces.get(j).annotations.get(k).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
								} catch (NoObjectNameException e) {
									e.printStackTrace();
								}
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).annotations.get(k).args != null) {
									for (int l = 0; l < this.sourceDatas.packages.get(i).interfaces.get(j).annotations.get(k).args.size(); l++) {
										if (l != 0)
											val2 += ",";
										val2 += " " + this.sourceDatas.packages.get(i).interfaces.get(j).annotations.get(k).args.get(l);
									}
								}
								
								val2 += "</code><br/>";
								val += val2;
							}
							
							try {
								lst.put(LexiqueManager.getTerm("term.annotations", "output"), val);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.annotations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						// les méthodes
						if (this.sourceDatas.packages.get(i).interfaces.get(j).methods != null) {
							int constructorsElementsCount = 0;
							int destructorsElementsCount = 0;
							int normalsElementsCount = 0;
							HashMap<String, String> constructorsHashMap = new HashMap<String, String>();
							HashMap<String, String> destructorsHashMap = new HashMap<String, String>();
							HashMap<String, String> normalsHashMap = new HashMap<String, String>();
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).interfaces.get(j).methods.size(); k++) {
								String val = "";
								String key = "<code>";
								key += "<a href=\""
										+ this.sourceDatas.packages.get(i).name
										+ "."
										+ this.sourceDatas.packages.get(i).interfaces.get(j).name
										+ "."
										+ this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).name
										+ ".html\">"
										+ this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).name;
								key += "</a></code>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).since)
										&& !isStringEmpty(this.sourceDatas.version)
										&& this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).since.equals(this.sourceDatas.version))
									key += " " + AppearanceConfigUnit.getLabelNew();
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).shortDescription))
									val += this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).shortDescription;
								else if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).description)) {
									if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).description.length() < AppearanceConfigUnit.descriptionMaxLength)
										val += this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).description;
									else
										val += this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).description.substring(0, AppearanceConfigUnit.descriptionMaxLength) + "...";
								}
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isConstructor) {
									constructorsElementsCount++;
									constructorsHashMap.put(key, val);
								} else if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isDestructor) {
									destructorsElementsCount++;
									destructorsHashMap.put(key, val);
								} else {
									normalsElementsCount++;
									normalsHashMap.put(key, val);
								}
							}
							
							final String constructorsElementsCountS = new StringBuilder().append(constructorsElementsCount).toString();
							final String destructorsElementsCountS = new StringBuilder().append(destructorsElementsCount).toString();
							final String normalsElementsCountS = new StringBuilder().append(normalsElementsCount).toString();
							
							if (!constructorsHashMap.isEmpty()) {
								file += "<br/>";
								try {
									file += AppearanceConfigUnit.generateTable(destructorsHashMap, LexiqueManager.getTerm("title.constructors", "output") + " <span class=\"badge\">" + constructorsElementsCountS + "</span>");
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
							}
							if (!destructorsHashMap.isEmpty()) {
								file += "<br/>";
								try {
									file += AppearanceConfigUnit.generateTable(destructorsHashMap, LexiqueManager.getTerm("title.destructors", "output") + " <span class=\"badge\">" + destructorsElementsCountS + "</span>");
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
							}
							if (!normalsHashMap.isEmpty()) {
								file += "<br/>";
								try {
									file += AppearanceConfigUnit.generateTable(normalsHashMap, LexiqueManager.getTerm("title.methods", "output") + " <span class=\"badge\">" + normalsElementsCountS + "</span>");
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
							}
						}
						
						// les membres
						if (this.sourceDatas.packages.get(i).interfaces.get(j).members != null) {
							int elementsCount = 0;
							HashMap<String, String> membersLst = new HashMap<String, String>();
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).interfaces.get(j).members.size(); k++) {
								int modifiersCount = 0;
								String key = "<code>";
								String val = "";
								elementsCount++;
								
								key += this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).scope.getValue();
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).isStatic) {
									if (modifiersCount > 0)
										key += " ";
									key += "static";
									modifiersCount++;
								}
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).isFinal) {
									if (modifiersCount > 0)
										key += " ";
									key += "final";
									modifiersCount++;
								}
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).isConst) {
									if (modifiersCount > 0)
										key += " ";
									key += "const";
									modifiersCount++;
								}
								
								if (modifiersCount > 0)
									key += " ";
								try {
									key += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).type, ObjectsFinderSearchOrder.TYPE, false, this.sourceDatas.packages.get(i).name);
								} catch (NoObjectNameException e) {
									e.printStackTrace();
									key += this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).type;
								}
								modifiersCount++;
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).pointer) {
									if (modifiersCount > 0)
										key += " ";
									key += "*";
								}
								
								if (modifiersCount > 0)
									key += " ";
								key += this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).name;
								
								key += "</code>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).since)
										&& !isStringEmpty(this.sourceDatas.version)
										&& this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).since.equals(this.sourceDatas.version))
									val += " " + AppearanceConfigUnit.getLabelNew();
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).description))
									val += this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).description;
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).since)) {
									try {
										val += "<br/><b>" + LexiqueManager.getTerm("term.minVersion", "output") + " : </b>" + this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).since;
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).author)) {
									try {
										val += "<br/><b>" + LexiqueManager.getTerm("term.author", "output") + " : </b>" + this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).author;
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).deprecated)
									val += "<br/>" + AppearanceConfigUnit.getDeprecatedWarning();
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).annotations != null) {
									val += "<br/><b>";
									try {
										val += LexiqueManager.getTerm("term.annotations", "output");
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
									val += " : </b><br/>";
									
									for (int l = 0; l < this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).annotations.size(); l++) {
										String v = "<code>";
										try {
											v += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).annotations.get(l).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
										} catch (NoObjectNameException e) {
											e.printStackTrace();
										}
										if (this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).annotations.get(l).args != null) {
											for (int m = 0; m < this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).annotations.get(l).args.size(); m++) {
												if (m != 0)
													v += ",";
												v += " ";
												v += this.sourceDatas.packages.get(i).interfaces.get(j).members.get(k).annotations.get(l).args.get(m);
											}
										}
										v += "</code><br/>";
										val += v;
									}
								}
								
								lst.put(key, val);
							}
							
							final String elementsCountS = new StringBuilder().append(elementsCount).toString();
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(membersLst, LexiqueManager.getTerm("title.members", "output") + " <span class=\"badge\">" + elementsCountS + "</span>");
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).interfaces.get(j).name + ".html", file);
						this.filesCreatedCount++;
					}
				}
				
				// création des fichiers de méthodes d'interfaces
				DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Generating interfaces methods files");
				if (this.sourceDatas.packages.get(i).interfaces != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).interfaces.size(); j++) {
						if (this.sourceDatas.packages.get(i).interfaces.get(j).methods != null) {
							for (int k = 0; k < this.sourceDatas.packages.get(i).interfaces.get(j).methods.size(); k++) {
								String file = "<!DOCTYPE HTML>";
								file += "<html><head>";
								file += "<title>" + this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).name + "</title>";
								file += "<meta charset=\"utf-8\"/>";
								file += "<link rel=stylesheet href=\"../css/style.css\"/>";
								file += "</head><body>";
								
								file += AppearanceConfigUnit.getHeaderMenu(true);
								
								String[][] bcInfos = {
										{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
										{this.sourceDatas.packages.get(i).interfaces.get(j).name, this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).interfaces.get(j).name + ".html"},
										{this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).name, ""}
								};
								file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
								
								file += "<div class=\"page-header\"><h1>" + this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).name + "</h1></div>";
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).deprecated)
									file += AppearanceConfigUnit.getDeprecatedWarning();
								
								String labels = "";
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isConstructor) {
									try {
										labels += AppearanceConfigUnit.generateLabel(LexiqueManager.getTerm("term.constructor", "output")) + " ";
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isDestructor) {
									try {
										labels += AppearanceConfigUnit.generateLabel(LexiqueManager.getTerm("term.destructor", "output")) + " ";
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (!isStringEmpty(labels)) {
									labels = "<p>" + labels + "</p>";
									file += labels;
								}
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).shortDescription))
									file += "<p>" + this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).shortDescription + "</p>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).description))
									file += "<p>" + this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).description + "</p>";
								
								HashMap<String, String> lst = new HashMap<String, String>();
								if (!this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isConstructor
										&& !this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isDestructor) {
									try {
										lst.put(LexiqueManager.getTerm("term.returnValue", "output"), this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).returnValue, ObjectsFinderSearchOrder.STD, true, this.sourceDatas.packages.get(i).name));
									} catch (TermNotFoundException e1) {
										e1.printStackTrace();
									} catch (NoObjectNameException e2) {
										e2.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).ref) {
									try {
										lst.put(LexiqueManager.getTerm("term.returnRef", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).pointer) {
									try {
										lst.put(LexiqueManager.getTerm("term.returnPointer", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								try {
									lst.put(LexiqueManager.getTerm("term.scope", "output"), this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).scope.getValue());
								} catch (TermNotFoundException e) {
									e.printStackTrace();
								}
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).since)) {
									try {
										lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).since);
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).author)) {
									try {
										lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).author);
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).annotations != null) {
									String val = "";
									
									for (int l = 0; l < this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).annotations.size(); l++) {
										String val2 = "<code>";
										try {
											val2 += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).annotations.get(l).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
										} catch (NoObjectNameException e) {
											e.printStackTrace();
										}
										
										if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).annotations.get(l).args != null) {
											for (int m = 0; m < this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).annotations.get(l).args.size(); m++) {
												if (m != 0)
													val2 += ",";
												val2 += " " + this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).annotations.get(l).args.get(m);
											}
										}
										
										val2 += "</code><br/>";
										val += val2;
									}
									
									try {
										lst.put(LexiqueManager.getTerm("term.annotations", "output"), val);
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isStatic) {
									try {
										lst.put(LexiqueManager.getTerm("term.static", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).isAbstract) {
									try {
										lst.put(LexiqueManager.getTerm("term.abstract.f", "output"), LexiqueManager.getTerm("term.yes", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								if (!lst.isEmpty()) {
									file += "<br/>";
									try {
										file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
								}
								
								if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args != null) {
									int elementsCount = 0;
									HashMap<String, String> elementsHashMap = new HashMap<String, String>();
									
									for (int l = 0; l < this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.size(); l++) {
										int modifiersCount = 0;
										String val = "";
										String key = "<code>";
										elementsCount++;
										
										if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).description))
											val += this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).description;
										
										if (modifiersCount > 0)
											key += " ";
										try {
											key += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).type, ObjectsFinderSearchOrder.TYPE, false, this.sourceDatas.packages.get(i).name);
										} catch (NoObjectNameException e) {
											e.printStackTrace();
											key += this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).type;
										}
										modifiersCount++;
										
										if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).isConst) {
											if (modifiersCount > 0)
												key += " ";
											key += "const";
											modifiersCount++;
										}
										
										if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).ref) {
											key += "&";
											modifiersCount++;
										}
										
										if (this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).pointer) {
											key += "*";
											modifiersCount++;
										}
										
										if (modifiersCount > 0)
											key += " ";
										key += this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).name;
										
										if (!isStringEmpty(this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).defaultValue)) {
											if (modifiersCount > 0)
												key += " = ";
											else
												key += "= ";
											key += this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).args.get(l).defaultValue;
											modifiersCount++;
										}
										
										key += "</code>";
										
										elementsHashMap.put(key, val);
									}
									
									if (!elementsHashMap.isEmpty()) {
										file += "<br/>";
										try {
											file += AppearanceConfigUnit.generateTable(elementsHashMap, LexiqueManager.getTerm("title.arguments", "output"));
										} catch (TermNotFoundException e) {
											e.printStackTrace();
										}
									}
								}
								
								file += "</body></html>";
								new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).interfaces.get(j).name + "." + this.sourceDatas.packages.get(i).interfaces.get(j).methods.get(k).name + ".html", file);
								this.filesCreatedCount++;
							}
						}
					}
				}
			}
		}
		
		// création des fichiers de variables
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Generating variables files...");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).variables != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).variables.size(); j++) {
						String file = "<!DOCTYPE HTML><html><head>";
						file += "<title>" + this.sourceDatas.packages.get(i).variables.get(j).name + "</title>";
						file += "<meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).variables.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>" + this.sourceDatas.packages.get(i).variables.get(j).name + "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).variables.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).variables.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).variables.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						try {
							lst.put(LexiqueManager.getTerm("term.type", "output"), this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).variables.get(j).type, ObjectsFinderSearchOrder.TYPE, true, this.sourceDatas.packages.get(i).name));
						} catch (TermNotFoundException e1) {
							e1.printStackTrace();
						} catch (NoObjectNameException e2) {
							e2.printStackTrace();
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).variables.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).variables.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).variables.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).variables.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).variables.get(j).isConst) {
							try {
								lst.put(LexiqueManager.getTerm("term.constant.f", "output"), LexiqueManager.getTerm("term.yes", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).variables.get(j).pointer) {
							try {
								lst.put(LexiqueManager.getTerm("term.pointer", "output"), LexiqueManager.getTerm("term.yes", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (this.sourceDatas.packages.get(i).variables.get(j).annotations != null) {
							String val = "";
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).variables.get(j).annotations.size(); k++) {
								String val2 = "<code>";
								try {
									val2 += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).variables.get(j).annotations.get(k).name, ObjectsFinderSearchOrder.ANNOTATION, false, this.sourceDatas.packages.get(i).name);
								} catch (NoObjectNameException e) {
									e.printStackTrace();
								}
								
								if (this.sourceDatas.packages.get(i).variables.get(j).annotations.get(k).args != null) {
									for (int l = 0; l < this.sourceDatas.packages.get(i).variables.get(j).annotations.get(k).args.size(); l++) {
										if (l != 0)
											val2 += ",";
										val2 += " " + this.sourceDatas.packages.get(i).variables.get(j).annotations.get(k).args.get(l);
									}
								}
								
								val2 += "</code><br/>";
								val += val2;
							}
							
							try {
								lst.put(LexiqueManager.getTerm("term.annotations", "output"), val);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).variables.get(j).name + ".html", file);
						this.filesCreatedCount++;
					}
				}
			}
		}
		
		// création des fichiers d'annotations
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Generating annotations files...");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).annotations != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).annotations.size(); j++) {
						String file = "<!DOCTYPE HTML><html><head>";
						file += "<title>" + this.sourceDatas.packages.get(i).annotations.get(j).name + "</title>";
						file += "<meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).annotations.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>" + this.language.getAnnotationsPrefix() + this.sourceDatas.packages.get(i).annotations.get(j).name + "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).annotations.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).shortDescription))
							file += "<p>" + this.sourceDatas.packages.get(i).annotations.get(j).shortDescription + "</p>";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).annotations.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).annotations.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).annotations.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (this.sourceDatas.packages.get(i).annotations.get(j).args != null) {
							HashMap<String, String> argslst = new HashMap<String, String>();
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).annotations.get(j).args.size(); k++) {
								String key = "<code>";
								String val = "";
								
								try {
									key += this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).annotations.get(j).args.get(k).type, ObjectsFinderSearchOrder.TYPE, false, this.sourceDatas.packages.get(i).name);
								} catch (NoObjectNameException e) {
									e.printStackTrace();
									key += this.sourceDatas.packages.get(i).annotations.get(j).args.get(k).type;
									key += " ";
								}
								key += " ";
								key += this.sourceDatas.packages.get(i).annotations.get(j).args.get(k).name;
								if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).args.get(k).defaultValue))
									key += " = " + this.sourceDatas.packages.get(i).annotations.get(j).args.get(k).defaultValue;
								key += "</code>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).annotations.get(j).args.get(k).description))
									val = this.sourceDatas.packages.get(i).annotations.get(j).args.get(k).description;
								
								argslst.put(key, val);
							}
							
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(argslst, LexiqueManager.getTerm("title.arguments", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).annotations.get(j).name + ".html", file);
						this.filesCreatedCount++;
					}
				}
			}
		}
		
		// création des fichiers de constantes de préprocesseur
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Generating preprocessor constants files...");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).preprocessorConstants != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).preprocessorConstants.size(); j++) {
						String file = "<!DOCTYPE HTML><html><head>";
						file += "<title>" + this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name + "</title>";
						file += "<meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>" + this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name + "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).preprocessorConstants.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).preprocessorConstants.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).preprocessorConstants.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						if (!isStringEmpty(this.sourceDatas.packages.get(i).preprocessorConstants.get(j).value)) {
							try {
								lst.put(LexiqueManager.getTerm("term.value", "output"), "<code>" + this.sourceDatas.packages.get(i).preprocessorConstants.get(j).value + "</code>");
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						} else {
							try {
								lst.put(LexiqueManager.getTerm("term.value", "output"), "<span class=\"label label-info\">" + LexiqueManager.getTerm("term.none.f", "output") + "</span>");
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).preprocessorConstants.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).preprocessorConstants.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).preprocessorConstants.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).preprocessorConstants.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).preprocessorConstants.get(j).name + ".html", file);
						this.filesCreatedCount++;
					}
				}
			}
		}
		
		// création des fichiers de suitcases
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Generating suitcases files...");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).suitcases != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).suitcases.size(); j++) {
						String file = "<!DOCTYPE HTML><html><head>";
						file += "<title>" + this.sourceDatas.packages.get(i).suitcases.get(j).name + "</title>";
						file += "<meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).suitcases.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>" + this.sourceDatas.packages.get(i).suitcases.get(j).name + "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).suitcases.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).shortDescription))
							file += "<p>" + this.sourceDatas.packages.get(i).suitcases.get(j).shortDescription + "</p>";
						if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).suitcases.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).suitcases.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).suitcases.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (this.sourceDatas.packages.get(i).suitcases.get(j).content != null) {
							int elementsCount = 0;
							HashMap<String, String> ctlst = new HashMap<String, String>();
							
							for (int k = 0; k < this.sourceDatas.packages.get(i).suitcases.get(j).content.size(); k++) {
								elementsCount++;
								
								String key = "<code>" + this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).name + "</code>";
								String val = "";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).value))
									key += " (<code>" + this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).value + "</code>)";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).since)
										&& !isStringEmpty(this.sourceDatas.version)
										&& this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).since.equals(this.sourceDatas.version))
									val += AppearanceConfigUnit.getLabelNew() + "<br/>";
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).description))
									val += this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).description;
								
								if (!isStringEmpty(this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).since)) {
									val += "<br/>";
									try {
										val += "<b>" + LexiqueManager.getTerm("term.minVersion", "output") + "</b> : ";
									} catch (TermNotFoundException e) {
										e.printStackTrace();
									}
									val += this.sourceDatas.packages.get(i).suitcases.get(j).content.get(k).since;
								}
								
								ctlst.put(key, val);
							}
							
							final String elementsCountS = new StringBuilder().append(elementsCount).toString();
							
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(ctlst, LexiqueManager.getTerm("calypso.title.suitcases", "output") + " <span class=\"badge\">" + elementsCountS + "</span>");
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).suitcases.get(j).name + ".html", file);
						this.filesCreatedCount++;
					}
				}
			}
		}
		
		// création des fichiers de typedefs
		DebugManager.print("com.darksidegames.docMaker:Documentation@generate", "Generating typedefs files...");
		if (this.sourceDatas.packages != null) {
			for (int i = 0; i < this.sourceDatas.packages.size(); i++) {
				if (this.sourceDatas.packages.get(i).typedefs != null) {
					for (int j = 0; j < this.sourceDatas.packages.get(i).typedefs.size(); j++) {
						String file = "<!DOCTYPE HTML><html><head>";
						file += "<title>" + this.sourceDatas.packages.get(i).typedefs.get(j).name + "</title>";
						file += "<meta charset=\"utf-8\"/>";
						file += "<link rel=stylesheet href=\"../css/style.css\"/>";
						file += "</head><body>";
						file += AppearanceConfigUnit.getHeaderMenu(true);
						
						String[][] bcInfos = {
								{this.sourceDatas.packages.get(i).name, this.sourceDatas.packages.get(i).name + ".html"},
								{this.sourceDatas.packages.get(i).typedefs.get(j).name, ""}
						};
						file += AppearanceConfigUnit.generateBreadcrumb(bcInfos, true);
						
						file += "<div class=\"page-header\"><h1>" + this.sourceDatas.packages.get(i).typedefs.get(j).name + "</h1></div>";
						
						if (this.sourceDatas.packages.get(i).typedefs.get(j).deprecated)
							file += AppearanceConfigUnit.getDeprecatedWarning();
						
						if (!isStringEmpty(this.sourceDatas.packages.get(i).typedefs.get(j).description))
							file += "<p>" + this.sourceDatas.packages.get(i).typedefs.get(j).description + "</p>";
						
						HashMap<String, String> lst = new HashMap<String, String>();
						try {
							lst.put(LexiqueManager.getTerm("term.value", "output"), this.objectsFinder.renderObject(this.sourceDatas.packages.get(i).typedefs.get(j).value, ObjectsFinderSearchOrder.STD, true, this.sourceDatas.packages.get(i).name));
						} catch (TermNotFoundException e1) {
							e1.printStackTrace();
						} catch (NoObjectNameException e2) {
							e2.printStackTrace();
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).typedefs.get(j).since)) {
							try {
								lst.put(LexiqueManager.getTerm("term.minVersion", "output"), this.sourceDatas.packages.get(i).typedefs.get(j).since);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						if (!isStringEmpty(this.sourceDatas.packages.get(i).typedefs.get(j).author)) {
							try {
								lst.put(LexiqueManager.getTerm("term.author", "output"), this.sourceDatas.packages.get(i).typedefs.get(j).author);
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						if (!lst.isEmpty()) {
							file += "<br/>";
							try {
								file += AppearanceConfigUnit.generateTable(lst, LexiqueManager.getTerm("title.informations", "output"));
							} catch (TermNotFoundException e) {
								e.printStackTrace();
							}
						}
						
						file += "</body></html>";
						new OutputFile(path + "/elements/" + this.sourceDatas.packages.get(i).name + "." + this.sourceDatas.packages.get(i).typedefs.get(j).name + ".html", file);
						this.filesCreatedCount++;
					}
				}
			}
		}
	}
}
